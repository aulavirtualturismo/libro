                LA DESTRUCCION DEL PAPEL

                                  BOSTON Voz de origen cherokee que
                                  en lengua natal significa "caca
                                  grande.

                                  " ... Y descubrimos sin querer, esa
                                  puerta que lentamente se abre,
                                  dej�ndonos ver el prado donde
                                  relincha el unicornio ..."

                                  Al gordo, uno de esos que cuando va
                                  a la helader�a pide "Don Pedro,
                                  pero sin helado"

-Dej� de joder quer�s -dijo el Gast�n al Darwin- no tiene l�gica
pensar una boludez del tama�o de la tuya.

-Pero ten�s que tener en cuenta el golpe tremendo que significar�a
que desapareciera de golpe todo el papel.

Pero Gast�n se levant� del comedor, sabi�ndose mas tarado que Darwin
al consentirle discutir sobre el tema.  Ellos lo hac�an casi por
c�bala, constantemente, uno esgrim�a argumentos t�cnicos y el otro
los literarios, porque de eso no pasaban.  No ten�a otro motivo la
discusi�n que el mero placer de ver enfurecer al enemigo, en eso
eran rivales a muerte.

A Darwin, se le hab�a puesto ahora que Gast�n ten�a que inventar una
bacteria que se comiera el papel, para la que ya hab�a inventado hasta
el nombre PAPELOFAGUS MAGNIS, y los requisitos que deber�a reunir.
Con eso ten�an solucionada la revoluci�n.  Hasta ese punto aguant� la
coherencia del estudiante de gen�tica, pero no comedor de vidrio, que
escuchaba pacientemente, mientras el otro rascaba su barba incipiente,
mostrando como siempre que lo hac�a, el delirio interno que le nublaba
la coherencia, en el acto reflejo del rascado.

Gast�n intent� cuatro veces levantarse del comedor, lo aguardaban un
mont�n de frasquitos en el laboratorio, y unas ganas tremendas de
sacarse de encima al tarado del Darwin.  Por �ltimo se levant�,
esquiv� la derecha del compa�ero destinada a frenar el mutis por el
foro, astutamente, con un juego de cintura propio de Nicolino Loche,
y por �ltimo, trat� de no escuchar las palabras que lo segu�an desde
la retaguardia, como si no pudieran morir en el aire y tuvieran que
penetrar en sus o�dos como �nico cometido en este mundo.

-Vos no me entend�s -dijo el Darwin utilizando el bajo recurso dela v�ctima de la incomprensi�n, acompa�ando las palabras concontracci�n general del cuerpo, ca�da de hombros y cara de perroque volte� el jarr�n-

-�No jod�s?, �No me hab�a dado cuenta que no te entiendo! -lachicana mort�fera del Gast�n lo descoloc� al Darwin-

-Me est�s cohartando una excelente idea, pero no va a quedar as�,lo vas a tener que hacer, te guste o no -ya en tono amenazante,el pobre negro, al que todos ten�an por casi fronterizo-

La Moni le adivin� las intenciones ni bien lo vio aparecerpor la escalera del quinto subsuelo, con el pelo enmara�ado y labarba toda desprolija de rascarse inconcientemente y ya casi porolfato lo conoc�a, sab�a que le estaba por pedir la escupiderapor algo; le quedaban pocos segundos para inventar algo con quezafar del Darwin y su catarata de palabras.  Algo la ten�a quesalvar, alg�n problema afuera, uno de los chicos con dificultadesen la Porota, y mientras mas se preocupaba, menos le sal�a.  Y yalo ten�a encima al negro.  Tom� aire, esper� los dos segundos quela separaban de la situaci�n que deb�a tratar de manejar y abri�
los ojos y los o�dos.  Manejar la situaci�n, no dejarse llevaraguas abajo, en el r�o de las locuras.  Autocontrol.  AguanteMoni.

     El problema hab�a sido en el comedor, con el Gast�n, cuandoel Darwin se le sent� a la derecha para contarle eso que estabapensando.  La Moni cay� en la cuenta que era dif�cil no quererloa Darwin, pod�an sentirs e un mont�n de repulsiones a susactitudes, pero siempre era un ser querible.

-Raro vos volando -dijo la Moni, imaginando la que se le ven�aencima y alguna intervenci�n marihuan�stica en todo aquello- �Yque estabas pensando?

     La Moni sab�a que ese pi� que le estaba dando era susentencia de muerte, pero as� era ella, toda una madre.

-Inventar una bacteria, peque�a en volumen, pero importante en la
cantidad a producir.  No se olviden que lo de los mosquitos fueidea mia.  Esa bacteria 'Papel�fagus Magnis' se alimentar�aexclusivamente de papel, ya sea de celulosa, de cereales, satinado, etc. de todos los papeles que existen.

-�Y como van a inventarla?

-Necesitamos tiempo y el laboratorio trabajando a FULL, hay quecruzar las bacterias que se encontraron en la cola del Halleycon las mejores caracter�sticas, en conjunci�n con las quedestruyen los papeles en las bibliotecas...  Bien es sabido quehab�a bacterias en la cola del cometa �Me entend�s?, quesoportaron temperaturas extremadamente bajas, y extremadamentealtas.  Una situaci�n similar se les presentar�a en una supuestacompresi�n en una soluci�n gaseosa, en la cabeza de unos
misiles, dirigidos estrategicamente hacia lugares clave,predeterminados, donde se...

     El Darwin hizo un alto por los vaivenes de la cabeza de laMoni, como indicando que tanto no le entraba, am�n de habersequedado con parte del discurso en alg�n punto de la estratosferacomo siempre le ocurr�a.  Pasa que el pensamiento es mucho masr�pido que la voz, entonces Darwin siempre pensaba cuarenta ycinco cosas juntas, de las cuales dec�a nada mas que cinco,escap�ndose las cuarenta restantes.  Una l�stima.  Ahora dudabade seguir con los datos de la temperatura del cometa, o loslugares a los que habr�a que dirigir las bombas, o que tipo desoluci�n gaseosa deber�a ser, a cuantas atm�sferas de presi�n.No importa, ya saldr�a.

-�Pero vos te pens�s que es joda la gen�tica, que es solo cortarlos palitos y atarlos con rafia? -dijo la Moni en clara alusi�na los injertos en las plantas de c�tricos-

-No Moni, no me agarr�s la onda, lo que justifica todo son losfines, los objetivos que nos fijamos.  Si llegar�ramos aproducir las bacterias, o g�rmenes, o lo que sea; podr�amoshacer que desapareciera el papel que hay en todo el mundo, lost�tulos de propiedad, las actas de casamiento, las defunciones...

-�Para qu� quer�s que desaparezca todo el papel?

-�Como para qu�?, es volver a la Aldea Global, volver al trueque,al intercambio, no va a haber mas compromisos contractuales, nomas dinero, ni cheques, ni papeles de la bolsa, ni ninguna deesas mierdas.  Moni, vos me est�s cargando, �estamos o no
estamos juntos en esta? �Qu� mierda te pasa, est�s como el otroboludo ya? -dijo el negro, con una ligera coloraci�n rojiza enel rostro-

-Despacito con las palabras, que se termina toda la charla.  Si,seguro que estamos juntos, pero no me queda muy claro para ellado que me est�s corriendo.

-�Te imagin�s el banquero, due�o de toda esa cantidad de guita,
que de la noche a la ma�ana se queda sin nada? -dijo extasiadoel negro Darwin, casi como si lo estuviera viendo al tipo-

-A lo mejor no hac�s mas que ayudarlo a blanquear alg�nquilombito en negro que tuviera alg�n tufo a podrido -la Moni yahab�a optado por gastarlo-

-Si, est� bien, pero no importa.  �Te das una idea de la cara deesos pobres jubilados, buscando los �ltimos mangos para comprarla leche, sin darse cuenta que no necesitar�n mas dinero paranada? -ya con claros indicios de esquizofrenia-

-Encima le pens�s sacar guita a los jubilados.  Se nota que endefinitiva no sos mas que otra de las mierdas del gobierno.Deb�s ser un infiltrado.

-No Moni, te juro que no, eso que te dije era solamente para quete hicieras una idea de como ser�an las cosas despu�s de todoesto.  La cara de los pibes al enterarse que no hay mas clasesporque no hay mas libros -sin entender ya razones de �ndolel�gica, sin encadenar las palabras-

     El Darwin a esa altura no notaba que era objeto de burlas,de un paciente trabajo de grabaci�n, para que se divirtierantodos, �l incluido, cuando estuviera mejor apoyado sobre la
tierra (unos metros por debajo del nivel normal).  La Moni segu�a
con su May�utica Socr�tica de tratar de inducirlo a la verdad, desacarlo de ese convencimiento en otro de los miles de planes quehac�a el Darwin.  El �nico problema con los planes del Darwin,era bancarse los argumentos esgrimidos en su defensa.

-�Y las pobres monjas, frente a los libros de la biblioteca, quedesaparecieron sin que hubieran abierto uno solo? -volvi� aarremeter el Darwin con sus preguntas peyorativas-

-�Y el pobre gordo que estaba haciendo sus necesidades en elinodoro, y cuando estir� la mano derecha, como lo hace siempre,no encontr� nada, solo el admin�culo de madera -dijo la Moni,para tejer una similitud entre los ejemplos del Darwin y los deella, y entre el aparatito de madera, y la parte del cuerpo parala que estaba destinada el papel.

-Moni, ���vos tambi�n!!!, ���and� a cagar, quer�s!!!

-Pero no hay papel, Darwin -dijo la Moni ya con las l�grimasdemasiado visibles en el rostro-

     Pero no era f�cil de convencer el Darwin, por lo que lo queen apariencia pudiera significar una resignaci�n, una agachada decabeza por la inviabilidad de su proyecto, bien pod�a significarsolamente que se hab�a que dado sin aire para continuar laalocuci�n, o el comienzo del hambre, o el sue�o que lo hab�anganado.  Una pausa solamente, para recobrar energ�as, no hab�aque confiar en esas interrupciones.

     Por esos raros equilibrios de la ecolog�a el ecosistema, oesos nombres raros que tienen las cosas verdes esas, al ser queno le fue entregado cordura, le fue concedido en gracia, otras
armas con las cuales salir a enfrentar mundo como el nuestro (verla selecci�n natural de las especies).  A Darwin le fue entregadouno de los dones mas preciados; la modestia.  El ser modesto lepermit�a no enojarse con nadie, sino simplemente tomar como
cari�osas las actitudes desfavorables hacia sus proyectos, comojodas, y un par de sonrisas del Darwin hac�an que aflojaracualquier actitu� intransigente por parte del resto.  No era buenpol�tico, entre los banderistas no hac�a falta la pol�tica parallegar a sub-principal, pero si eran necesarias algunascaracter�sticas determinadas, como por ejemplo la tenacidad en laconsecuci�n de los fines.  Al Darwin eso le sobraba.  La locuraen la elecci�n de los medios a utilizar, pero sobre todo el honory el Darwin era un hombre de honor, si se cerraran los ojos, talvez se lo viera con la adarga al brazo y el viejo roc�n, opeleando contra los malos en las historietas del Toni, estuvierandonde estuvieran unos y otros, el Darwin estar�a con los buenosporque otra cosa no hubiera podido ser.  Estaba con losbanderistas de curioso que era, por esa conjunci�n en el alma quehace que como los dem�s, fuera mala palabra, esa actitud frente alas puestas del sol, ante la miseria humana, frente a laliteratura, al compromiso que es ineludible.  Hay gente que zafade eso, que puede encapucharse, escudarse en la OPINION PUBLICA ycon eso arregla todo, algunos no pueden y esos son los que secuelgan de los �rboles, soga mediante, o se hacen banderistas.

     La Moni tuvo un asalto de luz divina, crey� encontrar elorigen de tama�o nubarr�n en el cr�neo de Darwin, y se propusoactuar conforme a eso.

     El estaba subiendo las escaleras, en el bunker, el cuarentapor ciento de la edificaci�n eran escaleras; cuando la Moni se leacerc�.  El a�n estaba medio resentido con ella por lo de lafilmaci�n que usaron para re�rse de �l, pero trat�ndose de �l,era solo un enojo liviano, una bronca Light (para usar un t�rminode �poca), ella se disculp� en forma verbal y t�ctil,acarici�ndole la espalda.  El, de mas est� decirlo, no sac�grandes conclusiones de aquella caricia, ni de aquellas palabras.

Esas primeras palabras, siempre llevan encerradas las palabrasfinales, siempre esconden la intenci�n �ltima del poseedor de lasfrases, que es due�o hasta que las suelta, luego pasan a ser depropiedad colectiva (�socialismo c�mico, no?).  Ella prefiri�,entonces, llevar adelante el plan B, que era mucho mas frontal, yque casi prescind�a de esas molestas combinaciones de letras, queen esos momentos finales, molestan.

     Quien haya sido inquirido en las postrimer�as del coito,interpretar� lo antedicho.  La interrupci�n puede variar, perogeneralmente giran alrededor de la frase: �Me quer�s?, y ah� secort� todo.

     Ella lo abraz� y le pidi� perd�n en la oreja, le bes� lanuca, le peg� un par de trompadas en el est�mago para quereaccionara, y despu�s se hizo la ofendida, t�cnica esta que nofalla nunca.  Pronto �l se le acerc� para pedirle las disculpaspertinentes, sin saber demasiado porque estaba enojada; ella sehizo la que no escuchaba, �l se arrim�, ella coloc� las piernasen cierta y determinada posici�n, en que el pantal�n del uniformele marcaba mejor la linea de la min�scula bombacha, �l abri�
demasiado los ojos en esa direcci�n, haci�ndole saber sin querera la Moni que el pescado hab�a picado.  Pero que trabajo le hab�adado la pesca esta vez.

     Dos de los mas importantes debajo de esa tierra sindemasiadas expectativas, abandonaron las obligaciones por unospocos minutos, ya volver�an.  Prefirieron ir hasta uno de lasduchas del cuarto sub-suelo, las cuadras con las camas estaban amuchos escalones de distancia, all� ella comenz� el rito del ba�oy �l la sigui� mecanicamente.  Ella era demasiado linda parallevar esos pantalones demasiado embolsados, �l no terminaba desalir del aturdimiento, los o�dos le lat�an dentro del vapor dela ducha, el pecho se le reventaba, la panza le dol�a horrores ala altura del pupo.  Ella hizo todo lo que hab�a que hacer,primero lo ba�� e hizo que la ba�ara, todo demasiado r�pido peroa pesar de ello, sin perder el encanto.  Luego ella lo tom�, jug�con su persona, lo manejaba a su placer...

-Moni -dijo el negro despacito-

-�Que quer�s, negro -contest� la Moni juntando un poco de aireque no ten�a-

-�Me quer�s?

-Claro pelotudo, pero eso me toca preguntar a mi, las mujeressomos las que preguntamos eso.  �Porqu� lo pregunt�s? -dijo laMoni sin tomar las precauciones del caso-

-Porque no es tan descabellado destruir el papel... -dijo con lavoz como pidiendo permiso-

     La Moni se dio cuenta que hay gente a la que no se la puedeamar, por mas bueno que parezcan, son dif�ciles de seguir en el
tren del vuelo, y el Darwin era uno de esos, esperaba laaprobaci�n de ella fuera como fuese.

-Sab�s una cosa, pedazo de tarado -dijo la Moni demasiadocaliente, tanto por el enojo como por lo otro- es al pedodestruir el papel.

     Ella quer�a vengarse del gil de goma que la dejara en mediodel oc�ano de placer para discusiones de tipo geopol�ticas, yhab�a elegido pelearle en el terreno de �l.

     No hab�a dudas, �l no era un amante latino, tal vez fuera unpobre loco que en lo �nico que piensa es en sus delirios deinventor, como el otro escritor que cada vez que ten�a quemencionar su profesi�n dec�a inventor.

-No Moni, vas a ver como el mundo gira alrededor del papel, todose maneja con papeles, todo se transforma en papeles, todo tienesu siento en un papel.

-�Y para qu� te imagin�s que se inventaron los grandes sistemasde informaci�n, los grandes centros de almacenamiento de datos,las  computadoras?  ��Decime, contestame pelotudo!!.

     El Darwin se call� la boca, y reanud� aquella tarea queabandonara con el consiguiente enojo de la partenaire, tareamucho mas ardua que la de inventor, la de buen amante.