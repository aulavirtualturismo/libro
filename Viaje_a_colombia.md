#El realismo mágico 

Cuando leemos a García Marquez, mas precisamente, *"Cien Años de Soledad"*, tememos -por lo menos quienes vivmimos en estas latitudes- no lo extraño, sino todo lo contrario, tememos lo cercano que es Macondo a mi Baradero, en muchos aspectos.

Las prácticas semibrujeriles de Remedios la bella -creo que se trataba de ella, o eso quiero pensar- no se apartan mucho de la tía Elba y sus curas de empacho, por citar la mas cercana.

No pretendo con esto colocarme paralelo al delirio místico de Carlos Castaneda, ni los literarios de Cortazar, o Cabrera Infante, conocedor de mis escasos recursos literarios, tengo pretenciones mas "de cabotaje", tampoco pretendo hacer una tesis doctoral sobre el caracter curativo de ciertas prácticas, solo hablar de magia, que ya es bastante.

Porque como ya anticipara el siglo pasado el genial Althusser, "lo que no se puede explicar, se narra"

##La magia ocurre a diario, pero pasa desapercibida por cotidiana.

Es mío, tengo derecho a reclamar regalías por el uso de la frase anterior, todo esto no es mas de un juego de palabras con pretenciones de comicidad.

Dicho esto, con ustedes, los actores.

Tendemos a asignarle magia, o poderes sobrenaturales a aquello cuya complejidad nos abruma. Por lo general esto ocurre por desconocimiento de la "grilla de variables" que hacen a su funcionamiento.  Recuerdo con ternura la apertura de una radio a transistores por parte de mi papá a mis cuatro años -para renovar las pilas seguramente- y la graciosa explicaciíon de este a mi consulta sobre su funcionamiento, para reirse él.

- Los tipitos que hablan viven ahí adentro-

Gracias a esa curiosa variación de un sarcarmo, entiendo que mi aparato cognitivo tuvo una actitud puntual frente a las convenciones, las respuestas genéricas, la normalidad y tantos lugares comunes indiscutibles.  Tengo cierta incapacidad de tolerancia a la **Normalidad.**  Descubrí de manera autodidacta -seguramente mal- conceptos como amplificador de baja frecuencia, de alta, el objeto de un capacitor en la pata de un transistor.  Era preferible la magia de gente pequeña.

El ejemplo de funcionamiento mágico que mas a mano tengo es el del vuelo de un avión de pasajeros.  Mágico ao ojo indocto, cuando en realidad todo se reduce a un delicado equilibrio de variables, peso, empuje, superficie alar, densidad del aire, temperatura  y un etc. muy pequeño de valores adicionales.

Sin embargo, el solo hecho de estar apoyados en tierra firme, hace que tengamos una sensación mayor de seguridad transitando en auto por Acceso Norte de la Ciudad Autónoma de Buenos Aires, que por comodidad y convención llamaremos simplemente CABA; confiando ciegamente en verdades tan dudosas como:

- funcionamiento general y sincronización de semáforos
- averías o no de trenes delanteros o frenos de los diez mil autos colindantes
- presencia o no de baches de diferentes tamaños
- grado de alcohol en sangre -u otros tóxicos- en conductores colindantes
- cumplimiento de protocolos en agentes del orden respecto de disparar armas de fuego -asumiendo ingenuamente que no consumen alcohol u otros tóxicos-
- conocimiento y observancia de esos conductores colindantes de las normas de tránsito vigentes
- posibilidad de lleguada de un mensaje a ninguno de esos conductores de un tercer(e) sobre "salir esa noche", que lo fuerce a lectura compulsiva, o video escatológico, o chiste, que distraigan milisegundos sus ojos del "horizonte artificial"
- llamadas de ex
- horas de sueño real de todos y cada uno de esos individuos.

El Etc es demasiado largo y ya se dentra demasiado en terreno filosófico, como dice Darío Z, todos debemos pensar al menos una vez a la semana en nuestra finitud.

Entendemos luego la famosa frase que sostiene que **"Dios está en todos lados pero atiende en Buenos Aires"** hace referencia a que como en la guerra de Troya, está mirando desde algún túmulo y corrigiendo imperfecciones sobre la marcha, otra explicación no existe a deseso diario de mil personas por accidentes.

Eso es mágico y no el vuelo de un Airbus 380.

Los que saben de aeronavegación sostienen con argumentos muy interesantes, que para que un avión se caiga, deben de ocurrir las coincidencias del las burbujas del Gruyere.  Para que podamos ver de un lado a otro de la porción de gruyere, tienen que alinearse ocho burbujas.  Para un accidente aéreo, sostienen que tienen que darse ocho errores en línea.

Analizando el avión en vuelo y el viaje por el Acceso Norte en hora pico, cada uno haga sus cálculos de burbujas y magia.

Toda esta intro solo sirve de corolario para decir que en Colombia, esto se agrava.

### El Viaje a Colombia
Utilizamos Airbnb para tomar un dpto en Santa Marta.  Luis y Karen fueron cordiales, atentos y expeditivos al máximo.  Me sorprendió una ambiguedad en las respuestas a consultas de tiempos de viaje en colectivo de Santa Marta hasta el Parque Tayrona, que rompió esa lógica.

La sorpresa no viene de esa respuesta sola, sino que me alertó sobre respuestas similares sobre el mismo tema, un viaje por carretera no tiene una respuesta cierta.

- Puede demorar dos horas o dos y media, o tres, tal vez mas.

Al notar que el margen de error es muy por encima del 10% esperable y mas cercano al 100%, surge inmediatamente la pregunta.

- ¿Porqué?
- Accidentes por lo general.  Fue la respuesta.

Hasta no estar en la ruta, subidos a esa "Buseta", no entendí a que se refería.

Las normas de tránsito tal cual las conocemos, son ligeramente obviadas, digamos en honor a la poesía.  Es lógico y hasta esperable que en este rincón del mundo no se maneje con corrección.  Pero no a este extremo.  El derecho habla de "lagunas" al existir legislación respecto de algo.

Aquí en cambio estamos en presencia de una suspensión temporal, como la que opera en los casorios sobre el uso de armas de fuego, consumos tóxicos o sexo con consanguíneos en segundo grado.

Estamos en presencia de una Pausa, como la doble barra vertical en los dispositivos de música.

Subimos a un ómnibus pequeño, un bus, pero que los colombianos femeinizan por encontrarla mas pequeña -parecen machistas- y llaman tiernamente "buseta"

La buseta transita delante de la escuela a 20 km/h. lo que me llama la atención, pero sale de la ciudad y pareciera ponerse el pañuelo de rambo.  Descubrimos que suspension temporal de las leyes de tránsito opera en todos los órdenes, como tipos de vehículos , coniciones ambientales.

En Cartagena habíamos ya notado cierta "polusión sonora", primero inadvertida, el doble pitido tanto de taxis como de motocicletas.

Con el correr de las horas descubrimos que eran bienintencionada gente que ofrecía sus servicios.

Pareciera existir cierto código no escrito

- pip pip = Motociclista ofreciendo taxi
- pip pip pip = Idem anterior pero urgido economicamente
- pip pip PEEEEE pip = moto cruzando taxi, cercanos a la colisión
- PE PE  = Taxi ofreciendo sus servicios
- pip pip indevinido = Entepreneur

Con el paso de los días se adquiere un odio prolijo y negligente por esta actitud.  Está claro que matar a una de estas personas es delito.

Se agrava, puntualmente en Santa Marta, ya que en el casco céntrico, las tiendas textiles parecieran competir por calidad y voluminosidad del volumen sonoro con el que ofrecen prendas, por caso, calzones o jeans.

Juro haber visto personalmente una consola con un joven moreno disfrazado de DJ, intercalando increpaciones a comprar con ballenatos y bachatas.

El sonido este se superpone y convive con los anteriores, a los que se suma el diálogo de vendedores ambulantes y público general que debe interactuar con diálogos del tipo

- De que sabor quiere el helado mi reina

Pregunta el vendedor a una cliente femenina, que siempre es reina.

Uno puede sentirse tentado a pensar que esto dentro de la tienda mejora o se reduce.  Error.  Empeora.

Pareciera operar una competencia inter sectores dentro de la tienda: 

Blanco Vs. Ropa de dama

Ambos con su propia música, su propio DJ y arengas a compras.

Volviendo a la buseta que abandonó el casco céntrico, descubrimos con horror que al contrario de bip bip, cual correcaminos, su sonido por el contrario es mas parecido a BUUUU BUUU -posiblemente debido al tamaño-, y lo pone en práctica al pasar en doble fila, en una curva, en una montaña, a dos camiones, acantilado, diez pasajeros.

El mesanje oculto al que avanza de frente, en este extraño idioma morse es:

- Corransé que todos vamos a morir.

Llegamos al final de esos 2 segundos y no pasó nada, el chofer sigue charlando con acompañante, intercalando tres veces por frase la palabra "marica", ningún local parece temer a la muerte y lo mas extraño aún, ninguno de los prolijos europeos que son varios.

Miran hacia adelante, estiran cuello, se miran entre si, levantan los hombros "color local" pensarán.

Nadie grita, nadie lleva manos a la cabeza.

La magia existe.



