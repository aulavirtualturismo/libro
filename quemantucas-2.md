#Sarcasmo Castrense.

Altivos y valientes marchan los últimos hombres del general Jürgen Slamastik,
bah, hombres.  Eso.

Merma la soldadesca merced a la última hambruna.  

Hambruna que algunos atribuyeron a una iniciativa oficial por reducir el peso corporal promedio de la tropa -mediante una dieta magra en proteínas- cuando en realidad todo fue una **desinteligencia logística**.

Enflaquecen filas y columnas, cual hoja de cálculo, víctima de troyano hindú.

La locura, la envidia malogran la moral de la tropa.

Detalles nimios -al vulgo- es motivo de queja histérica, que los **entallados** uniformes del enemigo, que el **corte** de las calzas, que "El rosa chicle de los pañuelos", que "las botas altas durazno", ofenden por lo menos el intelecto del General.

No menos importante son la cobardía, el Netflix o el candycrush, distractor de los buenos hombres.

A las frecuentes deserciones se sumó el grave incidente del batallón perdido en la espesura del monte jugando a __La Escondida__.  

Apresurados análisis adjudican el error al alarde -posiblemente excesivo- del uso del camuflaje, otro punto de vista, a una impericia cartográfica, cuando lo mas probable fuera abuso de tóxicos lúdicos de parte de la oficialidad al mando del grupo.

Aún suele escucharse la voz del Primer Teniente Patricio Lamondiola, gritar al poniente nombres al azar de integrantes del batallón, con la vana esperanza de verlos aparecer detrás de un tronco añoso o un helecho serrucho.  Teatralizando la mirada fija, concentrado, aindiado de ojos.

- Pietrabuona, Dani Alves.  Resistencia, Chaco!  -¡Lo vi soldado!

- Arroyo Maldonado, Luis Enrique.  Monte Quemado, Santiago del Estero.  -Paso al frennnnn... te.  De frenteee.  __'Mar!__.

- Basavilbaso, Anibal.  Localización IDEM.  Salga.  Es una orden.

Trata de ahorrar energía evitando repeticiones cartográficas, por lo que agrupa los soldados de las mismas localidades.

El General Slamastik comanda algo mas que despojos de lo que otrora fuera un ejército invencible, ejemplo de gallardía en la región.  

Cuentos, poemas, óleos y hasta piezas musicales aun recuerdan proezas bélicas y amores dejados al paso de la tropa.  A un suboficial de este regimiento se le adjudica una zamba compuesta para un noviazgo breve, antes de la batalla.  Dos horas.  El noviazgo.  La batalla duró algo mas.  

Ese preciso esquema afectivo que algún sector de la clínica denomina __amor express__.

La zamba __"La Humectada"__ 

Endecasílabo centrado en la llegada de la niña al batallón, en medio del aguacero. 

Escuchando la zamba es casi verla, remera roja con un Círculo y una "A" negligente y tangencial, pañuelo verde proderechoso, a tono con el cabello.  Mirada mas firme que toda la tropa.  Bandera de cuadritos multicolor, cantanto un totalmente extemporáneo:

> "... Y tu vendrás, marchando junto a mi... 
> la luz de un rojo amanecer... "

Marchan al asedio final de la fortificada ciudadela de los **quemantucas**,
pueblo célebre entre sus enemigos por su hostil **comercio verbal**, su filosofía chupaunhuevista, su literatura idealista y su incipiente, aún, __turismo gastronómico__.

Llama poderosamente la atención a los sociólogos la práctica que llaman __Filantropía de lo Ajeno__, de los quemantucas.

Encuentran los locales, un extraño placer en la bajeza discursiva hacia los pueblos vecinos, en la chanza grosera que tantas vidas cobrara en *propios y ajenos*, cual teucros y aqueos a los pies de **la bien amurallada Ilión**.

Placer que atenta con los planes oficiales de incrementar el **Turismo Receptivo**.  

Discurso etnocéntrico propio de popular en estadio de fútbol, que a relaciones internacionales entre naciones vecinas civilizadas, que desoye evidentemente mandatos estilísticos del decoro y la prudencia como la transcripción de un email reciente. 

>"Reclamamos enérgicamente a los negros de mierda de nuestra nación vecina, cejen en su intromisión política reclamando por presos políticos, puesto que estos son unos indios cagados de hambre y negras culo roto..."

La soldadezca de Slamastik abandonó la comodidad de sus hogares para cobrarse la innoble afrenta de los quemantucas.  

Los lenguaraces trajeron veneno en sus palabras, advirtieron a los _iroqueces_, ojos enrojecidos, tartamudeos y altos en el relato que denotan ofuscación o abuso de tóxicos:

>"Ellos decir Boston significar **CACA GRANDE**"

Hecho que despertara **"preciosas y esperables lealtades"**, la leva reclutó lo mejor de las juventudes locales, entre cuarteles de bomberos, conservatorios musicales, clubes de rugby, claustros universitarios y casas de degustación de glandes, para lavar el honor con sangre enemiga.

Las tropas leales -lo que queda de ellas- proponen al general un duelo de gallito interdental, de hip hop o BMX y evitar con ello la lucha franca y directa.

Cabecea negativamente el general Slamastik, en creciente y entendible enojo hacia si mismo, observando franqueza en los ojos de los soldados y piensa:

> "Lo dicen en serio."

- ¿Quién me manda a mi con estos pelotudos? -Se autoamonesta con leves y sonoros golpes de la cara interna de la mano derecha a la frente, mientras eleva los ojos al _ sol hiperiónida_, no en busca de auxilio celestial, sino evalúa la alta radiación solar por recordar el olvido de la gorra y el protector solar 40, que cubra su alopecia crónica.

Brota la negra ira del general, en progresivo degradé, Recorre estados diferentes desde: el lógico y esperable: **"son chicos"**, pasando por **"tortura seguida  de muerte"** hasta llegar finalmente y como es frecuente; a la **duda vocacional**, recordando la repetitiva sentencia de la finada madre:

>"Tito, cuando seas grande tenes que ser service de PC"

Y su correspondiente amonestadora autorespuesta del __superyo__.

>"¿Porque no le hice caso a mamá?"

Duda el general asomado en lo alto del peñón, si avanzar con el pronto amanecer, mientras inspecciona inconscientemente el orificio nasal izquierdo, con el índice de la mano correspondiente. 

Observa __inconscientemente__ el resultado de la pesquisa, apelotona, amasa __-tanteando viscosidad y solidez-__ el contenido entre pulgar e índice, mirada perdida en el horizonte, Ojos húmedos a la aurora de rosáceos dedos.

Ojos achinados. ¿Miopía? ¿Basurita que trajo el viento o lisa y llana duda? 

- ¿Aprovechar la distracción del enemigo a la hora del **"Bailando por un sueño"** O esperar el sueño profundo?

Duda el general.  Analiza la opción de intervenir en la señal satelital con hackers espías, intervenir directamente el contenido y trocarlo por publicidades generosas en glúteos femeninos.

Hasta podría pasar desapercibido.

Se debate, sopesa, estima en su Yo el envío un Grupo Comando que agregue __LSD__ a la red de agua potable, mas desiste.  Se daría así la extraña paradoja del enemigo convertido en amigo.

Sabe a su adversario numeroso, aguerrido y valiente, pero bastante flojo en lo *cognitivo* y algo *pajeritos* -piensa- al tiempo que estima esta falta intelectual, originada en alguna bacteria del agua y cabecea afirmativamente, se auto festeja por la sagacidad y buen criterio.  Arriesga una respuesta por el lado de "sexo entre primos" o abuso de fumigación con agrotóxicos. 

Slamastik, gallardo, suelta el **"lastre escatológico"**, descarga la bolita entre sus dedos, al tiempo que una idea le nubla los ojos llamándolo a la realidad. 

Se observa a si mismo, de pié, perpendicular al horizonte artificial, gesto adusto, camisa impecable, botas lustradas, cinturón al tono, chaqueta de botones relucientes.  Vestigios de fluidos nasales en el puño izquierdo, que pasa desapercibido al público en general, pero que molestan sin embargo al 
(sic) general.  

Dificulta la lectura la homofonía entre el título de (general) Slamastik y el caracter (general) del público, que mas conviene atenuar que resaltar. 

Dirige la arenga a la tropa alrededor de la idea de la flojedad intelectual enemiga, al despuntar el alba.

En el verbo, el general rememora vidas de valientes patriotas anteriores,
gestas, historias de batallas, de renuncio, infidelidades conyugales, partidas de chupino de seis en fríos hangares y entrega al prójimo.

Habla asumiento que sus palabras hincharán pechos, despertarán necesarias valentías.  

Levanta la voz entonces.

Lo distrae la viserita en la nuca de un par de **opacos** de su tropa, suelta el gallito interdental para ganarse su simpatía y se chorréa la barba.  Cuelga el resultado de la acción errónea y duda si limpiarlo con el puño de la chaqueta o disimular.

Las risas lo hacen desistir de lo último.

Lo consume el personaje ante la proliferación del verbo.  Brotan lágrimas de exitación de sus ojos, mas escucha un harto familiar sonido escatológico, segundo plano, poco acorde a la solemnidad que exigen las horas, duele a su intelecto aceptarlo, demora en hacerlo espera (ruega) haberse equivocado, pero risitas festejaticias y rostros que contienen esas risas brotan en el flanco izquierdo de sus magras filas lo anotician de su error..  

>-Opacos -Piensa

Vuelve, dolorosamente la duda, llega nuevamente a la **crisis vocacional**.

Se debate el general entre la merecida _Pena Capital_ a los infractores
haciendo peligrar con ello sus esperanzas de victoria en la batalla, pero por otro lado se autoamonesta, los imagina detrás de una pieza de artillería vuelve sobre la díada **"actitud y aptitud"** y el temor se vuelve de tamaño continental, regional.   

Paradojas todas de difícil o imposible solución.

El coronel Ermenegildo Lamondiola, al mando de la brigada **"Unidos triunfaremos"**, reducen a los rebeldes y decide fusilarlos en juicio sumario, perdiendo así cuatro valiososos hombres y cuatro balas, -que echarán de menos en la batalla- de la columna de los afterofistas, célebres por su resistencia a la nocturnidad, sus estratagemas al Truco y su resistencia al sueño.

Vuelve a la táctica, planea, imagina un movimiento de pinzas, mas el nimio número de su tropa lo devuelve a la realidad, no le da para imaginar pinza de metalúrgia pesada, sino apenas una de depilar.  Pequeña.

Su tropa es reducida tanto en hombría, volumen y suministros, mas aun, después de la acción disciplinatoria que pasará -imagina- a los libros de historia como
"Fusilamiento en Quebrada de los Pedorros"

Sabe, sobradamente, de la inferioridad numérica, Los quemantucas los esperan con no menos de diez mil hombres, entre mecheros, enrolladores, mas la canalla pedestre, el general solo cuenta con medio batallón. ___Sobreestima___, posiblemente, el factor sorpresa.

Continúa la arenga, recupera algo del ímpetu perdido, Recuerda **Enrique V** de Shakespeare, quinto acto y lo que significaron los arqueros en la batalla.  
Recuerda su carencia, solo cuenta con un _cuatro_ de poca marca y dos centrales entre buenos y regulares, pero sabe, la defensa es un flan.

Cruzan a duras penas el faldeo que los locales llaman: *Merluserus Mount*, vadean el arroyo *"El Escrotal"*.  Divisan a lo lejos la fortaleza.

-¿Alguna pregunta? -Inquiere el General a sus hombres y a sabiendas de cometer un error -mas allá de la redundancia narrativa-

Avanza Gallardo uno de sus mejores cuadros, paso al frente, prensenta armas.

-Parte para mi General. Tengo una denuncia grave contra el Coronel Monticello.

-No tenemos ningún Coronel Monticello -Responde altivo el general-

-Agachate y conocelo -Responde con una carcajada el soldado tomándose "las verguenzas" entre ambas manos-

"¡Otra vez el pelotón, la puta madre!", piensa el general mientras trata de resolver el problema técnico aritmético de ser mayor el numero de soldados a fusilar que el pelotón de fusilamiento en si.

-Señor, o sea, Mi general, o sea, con su permiso -Se adelanta el soldado Mayorga- no quiero llevarme esta duda *al otro barrio* o sea, si vamos a morir hoy: ¿Porque viento en popa es exactamente lo contrario a viento en papo? O sea, NADA.

El soldado Mayorga pertenece a la estirpe de los oseadores.

Risitas festejaticias, nuevamente, escapan de lo que queda de las brigadas afterofistas, afectos como son a los tóxicos duros.

-Coronel -Interrumpe el Suboficial Mayor L'achilamp- Re no da atacar a las *cero setecientas*, estamos re de cara! -Cambio.

Uno de los pasos mas importante para el éxito es el reconocimiento de las falencias propias.  El General cabecea negativamente, acepta que __sacó__ Escuela de Guerra II con un cuatro, incluso hay quienes aseguran que "copió".  

Reconoce haber confiado demasiado en la brigada adiestrada en Counter Strike, especie de simulador de guerra, pero que aburguesa y genera demasiada adiposidad en la tropa. 

