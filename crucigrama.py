#import random

# Creamos una rejilla con "." que representarán espacios vacíos.
def crear_rejilla():
    rejilla = []
    for fila in range(15):
        rejilla.append([])
        for columna in range(50):
            rejilla[fila].append(' ')
    return rejilla

# Imprimos en pantalla la rejilla.
def imprimir_rejilla(rejilla):
    for fila in range(len(rejilla)):
        for columna in range(len(rejilla[fila])):
            print(rejilla[fila][columna], end = "")
        print()

# Intentará colocar la palabra. Devuelve True si salió bien,
# False si fallamos, por lo que tenemos que intentarlo otra vez.
def intenta_colocar_palabra(rejilla, palabra):
    # Determina la dirección del resultado-
    # Cambia el 8 por un 7 si no quieres palabras
    # al revés.
    direccion = random.randrange(0,8)
    if( direccion == 0 ):
        cambio_x = -1
        cambio_y = -1
    if( direccion == 1 ):
        cambio_x = 0
        cambio_y = 1
    if( direccion == 2 ):
        cambio_x = 1
        cambio_y = -1
    if( direccion == 3 ):
        cambio_x = 1
        cambio_y = 0
    if( direccion == 4 ):
        cambio_x = 1
        cambio_y = 1
    if( direccion == 5 ):
        cambio_x = 0
        cambio_y = 1
    if( direccion == 6 ):
        cambio_x = -1
        cambio_y = 1
    if( direccion == 7 ):
        cambio_x = -1
        cambio_y = 0

    # Encuentra el largo y alto de la rejilla
    alto=len(rejilla)
    largo=len(rejilla[0])

    # Creamos un punto aleatorio de inicio.
    columna=random.randrange(largo)
    fila=random.randrange(alto)

    # Comprueba que la palabra no vaya a sobrepasar el borde la rejilla.
    # Si lo hace, devuelve False. Hemos fallado.
    if(cambio_x < 0 and columna < len(palabra)):
        return False
    if(cambio_x > 0 and columna > largo-len(palabra)):
        return False
    if(cambio_y < 0 and fila < len(palabra)):
        return False
    if(cambio_y > 0 and fila > alto-len(palabra)):
        return False

    # Ahora comprobamos que no haya otra letra en tu camino.
    columna_actual = columna
    fila_actual = fila
    for letra in palabra:
        # Asegúrate de que esté vacía o que ya tenga la letra correcta.
        if rejilla[fila_actual][columna_actual] == letra or rejilla[fila_actual][columna_actual] == '.':
            fila_actual += cambio_y
            columna_actual += cambio_x
        else:
            # Oh! Una letra diferente ya está aquí. Fallo.
            return False

    # Hasta aquí todo va bien, colocamos las letras.
    columna_actual = columna
    fila_actual = fila
    for letra in palabra:
        rejilla[fila_actual][columna_actual] = letra
        fila_actual += cambio_y
        columna_actual += cambio_x
    return True

# Esto llama a intenta_colocar_palabra hasta que acabemos. Podría repetirse eternamente si
# no es posible colocar palabra.
def colocar_palabra(rejilla, palabra):
    exito = False

    while not(exito):
        exito = intenta_colocar_palabra(rejilla, palabra)

# Creamos una rejilla
rejilla = crear_rejilla()

# Colocamos algunas palabras
colocar_palabra(rejilla,"osopanda")
colocar_palabra(rejilla,"pez")
colocar_palabra(rejilla,"serpiente")
colocar_palabra(rejilla,"puercoespín")
colocar_palabra(rejilla,"perro")
colocar_palabra(rejilla,"gato")
colocar_palabra(rejilla,"tigre")
colocar_palabra(rejilla,"pájaro")
colocar_palabra(rejilla,"caimán")
colocar_palabra(rejilla,"hormiga")
colocar_palabra(rejilla,"camello")
colocar_palabra(rejilla,"delfín")

# Imprimimos todo
imprimir_rejilla(rejilla)