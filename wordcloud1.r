library(ggplot2) # Data visualization
library(readr) # CSV file I/O, e.g. the read_csv function
library(wordcloud)
library(tm)

mydata <- readLines("macri.txt")
mdcorpus <- Corpus(VectorSource(mydata))

preproc <- tm_map(mdcorpus, content_transformer(tolower))
preproc <- tm_map(preproc, removePunctuation)
preproc <- tm_map(preproc, removeWords, stopwords("spanish"))
preproc <- tm_map(preproc, stripWhitespace)

bolsa <- TermDocumentMatrix(preproc)
enmatriz <- as.matrix(bolsa)

ordenado <- sort(rowSums(enmatriz),decreasing=TRUE)
num <- data.frame(word = names(ordenado),freq=ordenado)

wordcloud(num$word,num$freq,min.freq=3)