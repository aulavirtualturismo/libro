Toda génesis es mito, el método del conocimiento científico es mitología, criterios de veracidad o autenticidad, al menos laxos.

Mitos que demoran en irse, o se van y vuelven. En ciertos estados de la unión, se enseña la teoría evolucionista, muy debatidas y resistida, a la par de el antiguo testamento, que asegura que el mundo tiene cinco mil años.

Cuando los titanes Prometeo y Epimeteo trabajan en el encargo del largovidente Zeus, inventar animales y humanos -ya que están todo el día jugando a la play-, el último descubre que sin fuego no van a ir muy lejos y decide desoír al pedante Zeus y baja al mundo de los mortales con una bracita escondida en su bastón de rama de hinojo.  

Lo que nos llama poderosamente la atención ya que imaginamos a los titanes
enormes y pesados, cosa que entra en conflicto con la idea de una rama de
hinojo que encima, se está quemando por dentro.

Licencias poéticas aparte, la sola idea de contravenir a los dioses los
vuelve humanos.  En la génesis humana ya está grabada la rebeldía y la
ruptura de reglas, casi en su ADN.

Los mitos no son verdaderos ni falsos, son mitos, una forma interesante de
explicar lo inexplicable, como bien sugiere Luis Althuser "aquello que no se
puede explicar, se narra"

Podemos correlacionar dos hechos independientes pero cercanos en el tiempo y deducir de ellos una correlación, como pregona el pensamiento mágico.  Invocando a un dios se apiade de nosotros y acabe la sequía, o elimine el cáncer, o ... Pensamiento mágico.

Veremos como este concepto vuelve a lo largo de la historia, recurrentemente.

Por robar el fuego del Olimpo, Prometeo es castigado a un eterno sufrimiento,
encadenado en el Cáucaso, un águila devora todas las noches el hígado, que
se regenera -por su condicion de inmortal- diariamente.  Lo que nos lleva a
replantearnos el concepto de eternidad.

¿De que sirve la eternidad si es para el eterno sufrimiento?  Los dioses nos
envían desdichas a los hombres para divertirse con sus canciones, sugiere Homero en la Ilíada.

¿Es mas aterrador el sufrimiento eterno o el placer eterno? ¿No será aterrador el sustantivo de eternidad en sí y no su calificativo?

En tanto que Prometeo repite eternamente el sufrimiento, Epimeteo no es menos y
recibe de Zeus una muñeca, **Pandora**, como en la peli **Blade Runner**, tan
hermosa como tonta, con una **bolsa** que los siglos posteriores llaman **Caja** -vaya uno a saber con qué intención poética- con la puntual prohibición de abrirla.

Nada mas interesante que la prohibición, que esta historia quiere reducir a un defecto femenino -Pandora era tan hermosa como tonta- cuando bien sabemos amigos, que la la curiosidad que despierta la prohibición es mérito compartido por todos los géneros y nacionalidades, hay un cuento interesantes sobre baskos.  

>- ¿Como meter cincuenta baskos en un ascensor? 
>- Diciéndoles que no van a entrar

Otro dios algunos siglos mas tarde toma el manzano que había permanecido por siglos en el jardín de las Hespérides y se lo lleva al paraiso terrenal, prohibiéndoles a Adán y Eva comer de él.

Como imaginarán, lo primero que hace Pandora es abrir la bolsa, donde Zeuz había encerrado todos los males para la humanidad.  Salieron todos los males, las enfermedades, las futuras penurias de la humanidad.

Epimeteo rápido alcanzó a cerrarla para que quede guardada **la Esperanza**.

Curiosa variación del pecado original, una mujer, todos los males de la humanidad se los debemos a dos mujeres.  Como curioso es averiguar como hicieron los griegos para robar conceptos de una religión varios siglos posteriores.

Me interesan resaltar algunos puntos:

Los mitos no son verdaderos ni falsos

No hay Titanes mujeres en la historia, ellas están condenadas a ser tontas, débiles, pequeñas, con papeles bastante irrelevantes en todas las historias, salvo para hacer el mal, causan todos los males del mundo.

Los dioses se divierten enviándonos desdichas para que tengamos motivos para
cantarlas.

Como moralejas, tenemos miles, la mas evidente es el mensaje esperanzador, por muy dura que sea, la vida merece vivirse.
Otros mas ocultos, nos muestran a seres disconformes con los mandatos divinos, generalmente mujeres, que quieren salir del paraiso de la felicidad eterna.

El esquema de **"no toques nada que así estamos todos bien"** es mucho mas viejo de lo que parece a simple vista.  Antes eran dioses, ahora neurocirujanos y doctores en ciencias polítícas, tan llenos de títulos en universidades europeas, como segundos de "Aire televisivo", casi podemos decir "**dioses**" que nos explican muy didacticamente -como Zeus o Jahve- que no toquemos nada nosotros, que no somos *personal capacitado* para hacerlo, que este es el mejor mundo de los posibles.

Lo que era una cuestión de Fe, ahora es cuestión de *Otra* Fe.

La institución de la circunsición marca un "nosotros" vs "ellos", dice un antropólogo, solo que la gran división se mantiene oculta por demasiado evidente como "la carta robada de Poe"

Nos debatimos contra el pueblo elegido, cuando en realidad ni el enorme grupo humano excluido nota la exclusión.  Las **mujeres**, que nunca serán circuncidadas y esta institución las deja fuera de toda discusión.

Conociendo las capacidades de ese grupo humano, las mujeres, frente a los hombres, sospecho que los dioses pactaron con los últimos algo para mantenernos en el oscurantismo.  

¿Y si desobedecemos y las dejamos al mando?
