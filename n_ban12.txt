LA MAQUINA DE NO DOLER

"Lo �nico que se consigue perfeccionando un mal sistema, es forzar al enemigo a perfeccionarse."
             Juan, el Ermita�o.


Darwin, (ahora el fugitivo), no hab�a reparado en esa extra�a ausencia de sonido.  Los canarios del vecino ya no cantaban, despu�s de haberlo hecho toda la tarde (a menudo el silencio es terrible).  Cuando quiso asomarse a la ventana, un golpe interno, mec�nico, le hizo agudizar el o�do de funci�n mejorada, el conocido chasquido del picaporte hizo que se agachara en un acto reflejo, tratando de alcanzar el 38 SP del caj�n de la mesa de luz, que lo esperaba atento como siempre; no lleg� a tocarlo cuando el uniformado de amarillo, ya le hab�a disparado con la pistola pl�stica del rayo rojo.  La aguja le penetr� entre las costillas 7 y 8, mientras un peque�o ardor le ganaba poco a poco el pecho.  El cerebro envi� una in�til y final orden cifrada a las cuerdas vocales "La Puta Madre", muy d�bil, pero de la boca solamente sali� un sonido gutural inaudible, irreductible a idioma alguno.

Descubri�, no sin algo de tristeza, la inutilidad del viejo y fiel 38 Special frente a la tela amarilla en la que ellos estaban enfundados.

Para cuando quiso recordar cual hab�a sido su crimen, era tarde, conoc�a demasiado bien el efecto de la aguja que le entr� en el pecho; el jugo lo hab�a paralizado f�sicamente, y a juzgar por la imposibilidad de coordinar los pensamientos, estim� que hab�a llegado ya al sistema nervioso central, parte del poder destructor.  Le quedaba muy poco por hacer, el hombre es optimista por naturaleza y cree que hay salidas.

Su crimen tal vez fuera una iniquidad.  �Una herej�a?  La cantidad de agentes del orden no era la usual.  �Era normal tener un revolver en la habitaci�n?

Un crimen pol�tico seguramente, o...  �Se llamaba en realidad Darwin? Fue su primera duda dolorosa.  Tres hombres armados con sopor�feros no salen nunca a cazar criminales comunes.  �Como se llamaban esos muchachos, los...  ?

En los dos segundos de semi inconsciencia que le quedaba, pens� todo aquello con lujo de detalles, y m�s.  Alz� la izquierda unos pocos tratando instintivamente de ovillarse para la ca�da, dado que el cuerpo se desplomar�a en esa direcci�n seguramente, y el aire se pobl� de un olor distinto, como saturado de albahaca, lejanamente familiar.

En apenas unos minutos estaba la ambulancia aspirando el cuerpo del delincuente; le conectaron los electrodos unidos a los delgados cables de colores.  Comprobaron la respiraci�n, el ritmo cardiaco y la actividad cerebral en mec�nicas y archisabidas maniobras m�dicas.  Nunca se sabe con esas cosas de las sensibilidades; no todos los individuos soportan esas dosis de la famosa droga para dormir el sue�o de los culpables.

Con El Fugitivo, las medidas de seguridad se hab�an extremado al m�ximo, al punto tal de parecer que no ocurriera otra cosa en el mundo, para los oficiales que conoc�an el PORQUE de la huida del fugitivo, y mas a�n para aquellos que conoc�an el VERDADERO PORQUE.

Cuando la ambulancia lleg�, el auto de la escolta qued� en la puerta atravesado, mientras escup�a uniformes amarillos que bajaban con armas de fuego, como los rifles largos, y otras que no eran de fuego pero m�s letales, parapet�ndose en las columnas, mientras en las cuatro esquinas del hospital, similares posiciones eran adoptadas por similares figuras humanas.

Adentro, el Fugitivo gozaba de una �ltima y poco duradera paz.

Las puertas de la ambulancia lanzaron de golpe la camilla erizada de cables y sondas, envuelta en uniformes de varios colores, blancos, azules y amarillos hacia la puerta vaiv�n del quir�fano, que de pronto aletearon para dejarla pasar, dado que escond�a debajo de las s�banas el cuerpo dormido del Fugitivo. 

Una vez adentro los hombres ocuparon las posiciones para las que estaban inteligentemente preparados por el sistema.  Solo quedaron dos de amarillo en la puerta principal, armados solamente con sopor�feros.  Toda informaci�n que anduviera por all� no deb�a hacerse desaparecer as� nom�s, por lo que no se les hab�a provisto armas mortales.  Los hombres controlaban que todos los  que pasaban, apoyaran las palmas en la digitolectora de la puerta. 

En la camilla, mientras tanto, el personal de sanidad, inoculaba l�quidos de colores dudosos en el cuerpo de Darwin, desenchufaban los terminales con cables de colores que no necesitar�an, y enchufaban los que s� har�an falta en esta segunda etapa.  Es aterrador para el indocto, la frialdad con la que manipulan a los seres (criminales o no).

El no era del todo alto, pero la impresi�n general era de un tipo de porte, el pelo enrulado, casi demasiado suelto para las convenciones, la nariz prominente, y ese pecado de la juventud delat�ndolo; las manos mostraban la rudeza del trabajo f�sico, pero impecables en la limpieza, como todo el aspecto general.  Aquel rostro peque�o, comparado con la nariz, como una figura que no termina de encajar, guardaba una simetr�a extra�a, despareja e incoherente.

Todos los presentes en la sala, conoc�an la historia completa de Darwin y de los suyos, sacada de los diarios que siempre magnifican las cosas para vender; las diferentes teor�as que tej�a el Poder Ejecutivo (con toda la coherencia de la frase) para explicar los errores propios que posibilitaron la acci�n de sabotaje que culminara d�as antes, en el desmantelamiento de una de las CENTRALES DE CONTROL POBLACIONAL, y con ello, varias cabezas del gobierno; teniendo que recurrir a alternativas extremas para controlar los des�rdenes sociales, que en circunstancias normales, no se hubieran producido.  Pero lo m�s importante era descifrar el pr�ximo objetivo de los Banderistas, que seguramente lo tendr�an, y los nombres de los cuadros superiores.  La �ltima pregunta era: �C�mo lo hab�an hecho?

Lo que m�s alarmaba al gobierno, de toda la confusa situaci�n, era que si un par de chicos hab�an llegado al punto de inutilizar una CCP era porque hab�an alcanzado un elevado nivel de capacitaci�n, violando las inviolables y extremas medidas de control del sistema, generando, por otra parte, sus propias medidas de seguridad.  Un doble problema.

Los m�dicos sentados en las banquetas de la cabecera de la camilla,    chequearon la hora de la pantalla 15:46:43, e hicieron correr el primer programa del sistema de computaci�n, preparado para estos casos de sondear en rincones del inconsciente.  C�modamente apoyados en las banquetas del peque�o e inc�modo quir�fano de campa�a, los tres tipos de blanco se miraron mientras el mas viejo se permit�a encender un cigarrillo ante el carraspeo de los otros, y la mirada sorprendida de los de amarillo, detr�s de los binoculares, maldijo esas miradas y la a letra "A" del teclado que no funcionaba bien.  Enviaron un par de est�mulos pre-codificados y observaron las respuestas, leyeron los datos mas a mano, y rescataron de la memoria del ordenador los otros datos cargados de antemano; una vez levantado todo, hicieron correr el programa principal.

El m�dico mas viejo tom� aci�n sobre las ventanas de la pantalla delante de �l.

El olor a Albahaca lo inund� de repente, con toda la fuerza; estaba corriendo y call� revolc�ndose sobre el alm�cigo que reci�n comenzaba a brotar, regado con esa paciencia que solo los a�os le hab�an dado a don Pedro, el abuelo, que  comenzaba a repetir el rito milenario de correrlo con la varita de mimbre; rito que con Darwin era la tercera generaci�n a la que lo hac�a.  Nunca el viejo le hab�a pegado a los hijos, menos lo har�a ahora con el bisnieto, sin embargo, repet�a las mismas palabras en italiano, tal vez por inercia. Quiso incorporarse, gastando en ello las ultimas fuerzas que ten�a reservadas para el llanto, �nica escapatoria a los chirlos en la cola, pero ya era tarde. La fuerza descomunal del abuelo lo elevaba por los aires en medio del casi/miedo al abuelo Pedro (que no pega chirlos), y toda la impotencia de no haber podido hacer nada, ni sacar las manos del barro, la cara embarrada, la ropa, mientras la abuela contemplaba desde lejos los setenta y cinco a�os de diferencia, apoyada en el bast�n que usaba de puro compadrona.  La carcajada estruendosa del abuelo que ol�a siempre a mate cocido, lo envolv�a, haciendo m�s penosa la derrota.

Darwin quiso hablarle al viejo, pero solo logr� soltar un par de sonidos sin sentido, sonando a mucha baba entre los escasos dientes, mientras el viejo le contestaba en tono de burla; conduci�ndolo de la mano, para que no se caiga, y la abuela alcanzaba el trapo h�medo que siempre ten�a preparado para este tipo de emergencias.  Pasaron delante de la jaula de los canarios, que acostumbraban alborotarse cuando pasaban por ah�, Darwin pregunt� algo al abuelo, en un c�digo que ya hab�an establecido, y el viejo contest� una afirmaci�n demasiado acentuada con la cabeza.  Algo record� acerca de canarios que le despert� la curiosidad, pero a esa edad las cosas se olvidan rapidez negligente.

Le lavaron las manos para merendar el mate con pan casero, y el abuelo se sent� en la cabecera de la mesa haciendo ruido al comer el pan mojado en la leche, y Darwin pregunt� porque el tero saludaba desde afuera, a lo que el viejo astuto contest� que es el animal mejor educado de todos, hasta en la vestimenta, porque anda siempre vestido de etiqueta, largando otra carcajada estruendosa (tal vez debido a su sordera).

El viejo le pregunt� por fin, sin levantar los ojos del mantel mientras la abuela retiraba la mirada de la leche hirviendo; "quienes estaban con �l en el sabotaje del CCP.  Darwin que no entend�a, ni las palabras del viejo, ni el reloj digital que titilaba en el margen superior derecho de la pantalla las 15:47:10, con el segundero increment�ndose de a un punto, y no supo que decir.

Not� que la imagen de los nonos lo acompa�aba hacia cualquier lugar que moviera la cabeza, al igual que el reloj.  En alg�n momento los rostros de los bisabuelos desaparec�a de la pantalla, cambiando por el de los tipos vestidos de blanco, en im�genes encimadas, uno con anteojos y el faso entre los dientes, insistiendo con las preguntas sobre quienes estaban con �l en la destrucci�n de la Central de Control Poblacional; qui�nes lo hab�an planeado...  Darwin not� que algo no funcionaba bien porque se le inundaba la garganta de l�grimas saladas, tal vez por haber perdido los rostros de los abuelos, y algo de aquella lejana inocencia.
De alg�n lejano agujero negro, llegaban las estrofas de esa vieja canci�n que lo hac�a llorar de solo recordarla, tal vez porque la hab�a aprendido en esa �poca donde todo se recuerda demasiado y es sabido cuanto duele recordar

               Tengo-una-banda domingueeeeera
               que siempre toca en la plaaaaaaza
               con una tuba grandooooooota
               yunos platillos de laaaaaaaa tael...
               ...el perro que mueve.....

Hab�a una voz demasiado familiar escondida en alg�n punto de toda esa oscuridad, el dolor de no poder volver, la angustia infantil, las l�grimas con los mocos, el cuerpo chiquito y fr�gil, y el gusto a los caramelos mediahora que solo son ricos a esa edad.

El mas viejo de los de guardapolvo blanco, opin� que a�n estaba demasiado sedado, la dosis de Cefaloca�na hab�a sido excesiva.  Deb�an esperar un par de pel�culas mas para sacarle algo.  El oficial de civil opin� que no dispon�an de tanto tiempo, a lo que el m�dico contest� alzando los hombros, haciendo uso de otro de esos permisos que conceden la fama y los conocimientos.  El sistema corri� un par de pruebas mas, sin obtener respuesta de Darwin que estaba cada vez mas lejos de la casa de los bisabuelos en aquel pueblo de provincia.  

Afuera, lo �nico que llegaba de esas proyecciones eran cifras, presi�n, pulsaciones y micro amperes emitidos desde alg�n rinc�n de un "humano hipot�lamo", ellos pulsaban alguna tecla por �nica respuesta; mientras tanto el Fugitivo oraba a todos los dioses que pueblan los cielos, por regresar a la casa de los Bisabuelos, implor� y llor� largamente, como acostumbran hacer los ni�os a su edad.

Darwin olfate� algo raro en su nombre, pero no prest� atenci�n por alguna raz�n inconsciente; trat� de ordenar esas intuiciones �nfimas para encontrar la Manija, esa de donde nos aferramos cuando todo es desorden.  Y not� tambi�n, que algo le dol�a cada vez que pensaba, o se�alaba, o mostraba, o  recordaba el famoso sistema que dec�an.  Una angustia como de muerte de un pintor le inundaba los ojos; seguramente que no deb�a pensarlo.

�Habr�a sido borrado el recuerdo del sistema, como esas ideas del fin del universo, o de la cuantificaci�n del tiempo?

Los m�dicos intentaron entretenerlo con otros videos, tratando de recuperar esos registros borrados con un sistema utilitario, mientras notaban que el cuerpo comenzaba a irradiar un mensaje cr�ptico que estaban esperando desde que lo ingresaron al quir�fano.  Un quinto programa se activ� instant�neamente, creando la barrera infranqueable contra ese particular mensaje, en esa frecuencia especifica (y no otra).

Era absolutamente imposible que el resto de los Banderistas recibieran el mensaje, era una torpeza de su parte pensar que pudieran atravesar las barreras del sistema sin la destrucci�n del fugitivo, aunque tal vez tambi�n por eso eran as� de tenues, casi como una formalidad, el mensaje deb�a ser activado con un �nico cometido coherente: el enemigo tratando de desactivarlo.  El mas joven de los que operaban hizo notar a los dem�s las 15:47:58, pero el viejo le dijo que el peligro estaba en dejarlo sin proyectarle nada, sin entretenerlo.

Mientras el sistema principal realizaba la b�squeda binaria de alg�n archivo de seguridad (que por error siempre quedan sin borrar); el segundo buscaba indicios en esos recuerdos aparentemente inofensivos, otro segu�a presentando est�mulos al cuerpo, buscando alguna respuesta del paciente en la camilla, y el �ltimo rechazaba las ondas de auxilio, en frecuencias y c�digos cambiantes (cosa  que tambi�n hab�a sido prevista por los hombres de guardapolvo.

En cierto momento, los globos oculares de Darwin se movieron mas de tres veces; el psiquiatra quit� los ojos del paciente mir�ndolo al de la derecha, que le contest� levantando las cejas por encima del l�mite superior de los anteojos, entendi�ndose esto como un "Puede ser".  Las l�neas del Electro recobraron el vaiv�n alocado y las ventanas del men� RECUERDO INFANTIL comenzaron a mostrar renglones de palabras separaras por guiones.

El m�dico m�s viejo, aceptaba las alternativas que enviaba el sistema, respondiendo afirmativa o negativamente, o simplemente tomando las que propon�a la m�quina por defecto:

     �PROCESO DE CONDENSACION AUTOMATICA        OK        SI/NO ?
     � PROCESO DE DESPLAZAMIENTO AUTOMATICO      OK        SI/NO ?
     � ANULACION PRINCIPIO DE REALIDAD                     SI/NO ?

La rodilla dejaba escapar un delgado y brillante hilo de sangre entre el barro, como una combinaci�n de colores medio morbosa, pero los cortes a esa edad no duelen, y menos en el fragor de un partido de f�tbol.  Los gritos lo convocaron a incorporarse de cuclillas, y a correr a pesar del corte y el tir�n en la rodilla; levant� la cabeza y distingui� el gris deste�ido del pantal�n de loneta con el n�mero 9 del "P�ldora", que desbordaba por la punta derecha, bastante exigido.  Alguno del mont�n alcanz� a gritar la orden de: "Echalo" y el nueve apenas toc� el esf�rico para hacerlo avanzar al cuatro contrario que lo marcaba (el negro).  Se escuch� la puteada del P�ldora por el codazo recibido, pero el negro qued� en el barro desparramado.

Darwin no necesit� mucho trabajo intelectual para verificar la desventaja en que se encontraban, a pesar de no poder distinguir a los compa�eros por las camisetas, hay un c�digo que se establece despu�s de los cinco minutos de juego, que fabrica la comuni�n del equipo.  Darwin dud� que el P�ldora le apuntara el centro a �l, por un lado, estaba como a un metro de entrar al �rea grande, todav�a, y por otro lado, ese mismo c�digo ya hab�a establecido tambi�n, que Darwin era un tronco, de esos a los cuales la pelota no se le pasa NUNCA.

Sinti� que le lat�an los o�dos. El cuerpo late los d�as de fr�o cuando uno corre; la transpiraci�n le flu�a salada por la nariz hasta la boca, con algo de gusto a barro.  Faltaban un par de pasos hasta el �rea chica, el nueve ya hab�a levantado la cabeza, como los que saben de eso, y lo atraves� con la mirada porque lo vio solo contra el "segundo palo".  Le peg� demasiado bien, como si no fuera cierto. La pelota describi� n amplio semic�rculo con el viento, y Darwin ten�a uno  o dos pasos a�n que salvar.  Los dio, se apoy� en la zapatilla demasiado resbaladiza de la izquierda, y se elev� viendo venir la pelota, con toda la fuerza, y le puso la cabeza, frente al arquero que le "sali�" a "achicar" en ese sector donde no hab�a nadie; y ese nubarr�n oscuro que lo tap�  �Con que le habr�a pegado? �Con la frente seguramente que no? �Cay� sobre alguno de los defensores?

De repente la barra lo abraz� durante el tiempo establecido en que se festeja un gol.  Y el dolor en la nariz.  Si, hab�a hecho el gol, y lo grit� y bail�, y se agarr� de los alambres como en la tele.  Cuando volv�a tuvo que disimular la alegr�a, no fuera que pensaran que era un boludo que nunca hab�a hecho un gol en su vida.  Pronto lo ganaron la verg�enza y la timidez, por el halago f�cil y tal vez fingido de alguno.  Hab�a cumplido alg�n secreto deseo de mucho tiempo.  Seguramente se comentar�a al d�a siguiente en tercer grado el gol del Darwin. 

Cuando el claroscuro de la tarde largaba sus �ltimos destellos rojizos, comenzaban las im�genes en blanco y negro, tornando la visi�n de la pelota en una mera adivinanza.  Y la democracia incipiente de los chicos, vot� la finalizaci�n del partido, sin que se levantaran las manos, se expresaran motivos o el resultado favoreciera a unos u otros, ese mismo c�digo dice que los partidos terminan cuando ya no se ve nada.

Parte del grupo, se encamin� para el lado de su casa, que era el lugar m�s cercano para tomar agua; picando la pelota en la calle, y entraron por �ltimo en la cocina de la casa de Darwin, donde la mami estaba cocinando la cena.  Tomaron toda el agua posible y se rieron de buena gana; luego le contaron a la mami que Darwin hab�a hecho un gol, pero la mami solo se volvi� para preguntarles mon�tonamente si ellos ven�an de romper el CCP, como si se tratara de otra de las travesuras a las que estaba demasiado acostumbrada.  Tal vez cre�a eso por la facha que ten�an, todos embarrados, y �l en particular, con la nariz y la rodilla ensangrentada.

El P�ldora, que momentos antes le entregara el pase del gol, ahora ten�a gruesos anteojos y apareci� nuevamente el reloj en la pantalla 15:49:40, para su extra�eza, y todos giraron los rostros mientras el Fugitivo solo quer�a que le preguntaran por su gol y no quienes estaban adem�s de ellos, no pod�an hablar de otra cosa, no pod�an sacarlo de ese lugar donde estaba tan feliz.  La pantalla del men� CONDENSACION, mostr� un error insalvable, imposibilitando recuperar el control de los sentimientos del paciente.

El Fugitivo volvi� a serlo porque las im�genes se le comenzaron a borrar y perdi� el status de autor de gol, en un mismo acto.  Los que antes vest�an pantalones cortos y remeras embarradas, ahora estaban impecablemente vestidos de blanco, frente a las pantallas.  La mami mezclaba preguntas de las heridas, con otras del sistema ideado para que la gente no se levantara contra el poder estatuido. 

Darwin tuvo unas extra�as sensaciones, de ser v�ctima de un robo de "Felicidad Infantil", de estar rodeado, de claustrofobia, y de su identidad, que no era precisamente Darwin el Fugitivo; pero algo lo hac�a rebotar a ese lugar donde le dol�a todo, donde el miedo a seguir vivo es peor al de estar muerto, por eso mismo, lo que le quedaba de cuerpo comenz� a irradiar mensajes m�s intensos, emitidos en forma intermitente, chocando en forma inevitable, contra la barrera tendida alrededor.  Cada vez que pensaba en su nombre, en el CCP, o los compa�eros, �hab�a una chica? Rebotaba contra esa masa verde viscosa, que lo hac�a girar sobre su eje, y lo aturd�a de colores hasta ceder al otro dolor.  Implor�, por �ltimo, ya sin demasiada convicci�n, esperando la felicidad de una pregunta sobre su �nico y ficticio gol.

Las voces de la mami se fueron desdibujando finalmente, el mantel de la mesa s� metamorfose� en s�bana, cubriendo el cuerpo familiar, de pronto un BIP del sistema indic� a sus m�dicos que termin� el juego y apareci� el mensaje esperado en la pantalla: INGRESAR PALABRA CLAVE, en color intenso, titilando al igual que el reloj. Solo quedaba la �ltima parte, el mas joven, sentado a la izquierda, activ� el programa que intentar�a romper la barrera de seguridad del Fugitivo; descubrir las caracter�sticas de la Password, luego, las dimensiones; mas tarde, las -seguramente- pocas y contadas oportunidades para ingresarla.

Algo ocurr�a mientras tanto, inadvertido por los de adentro.   El rostro hasta ahora imp�vido del enemigo del Sistema, tom� un color rojizo, como si hubiera recibido por segunda vez el pelotazo del P�ldora, y una contorsi�n muscular general bastante inusual en los alcanzados por los dardos de Cefaloca�na.  Un hilo de sangre sali� del orificio izquierdo de la nariz.  Esto alert� al personal, tanto los que entend�an, como los que no; y el recuadro que mostraba en el margen inferior izquierdo el t�tulo: CAPACIDAD DE SOBREVIDA 0%, cuando normalmente oscilaba entre 88 y 92 %.

El fugitivo hizo tres intentos consecutivos de muerte, logr�ndolo solo parcialmente en el tercero.  Fue en vida un ser hecho para ser golpeado, como los saleros de los bares.

Seguramente, pens� el m�dico viejo tratando de encontrar una explicaci�n innecesaria,  al intentar la primera password errada o fuera de t�rmino, se activ� un sistema de autodestrucci�n que no conoc�amos.  Pero estaba todo demasiado previsto para inquietarse, o rascarse la cabeza.  Unos l�quidos viscosos, muy distintos de la sangre, continuaron el trabajo de irrigaci�n cerebral, lo �nico que ellos necesitaban del cuerpo de la camilla.  Desde una m�quina port�til instalada al pie de la mesa, con sondas de diferentes colores.

El EEG, continuaba con la entrega de sus l�neas normalmente, mientras que todas las dem�s marcas estaban a cero.  El psiquiatra hizo notar al jefe la aparici�n un mensaje extra�o: DESTRUCCION EN DOS MINUTOS, pero el viejo le contest� que lo hab�an visto, que no se preocupara, que el imb�cil ese no molestaba mas a nadie.  Ese mensaje estaba cifrado y el sistema ya hab�a descubierto el c�digo; lo �nico que estaba haciendo es hac�rselo saber a los operadores el descubrimiento.

Pero el mensaje, estaba siendo irradiado en una onda muy alta una ultrafrecuencia ni so�ada por los que estaban a la par del cad�ver; para ser escuchado de lejos y atravesando todas las barreras, pero nadie le prest� la atenci�n necesaria; el sistema sab�a que tenia una sobrevida de 0 %, pero le enviaba el est�mulo de 82 %, para entretenerlo en eso de auto destruirse, y no rechazar las preguntas; por lo que el cuerpo del Fugitivo a�n cre�a que estaba vivo.  Y no solo vivo, se supo hombre, con todo el peso de sus 24 a�os; con todos los descubrimientos, la militancia pol�tica, viendo por �ltima vez el reloj 15:53:20, sabiendo que quedaba poco, todo el peso de la angustia, las ganas de seguir descubriendo, lo que acababa de empezar a dejar, las tramas de nuevos cr�menes para atentar contra la seguridad p�blica, los consejos de la mami, los canarios que no cantan y los hombres de amarillo y el aguijonazo certero en el pecho, la �ltima "imagen fatal de la demencia", la verg�enza y la deshonra de haber perdido otra vez, la albahaca y los nonos.  La aventura infantil de destruir un Centro de Control Poblacional, esos que inhiben la insatisfacci�n humana y la tristeza que produce el arte.  ERROR.

La muerte tambi�n es algo que le pasa a uno.

El reloj afuera parpade� 15:53:21 en el rinc�n reservado de la pantalla, el viejo balbuce� una blasfemia de alegr�a en una lengua tal vez inexistente, lo que fuera del cuerpo de Darwin emiti� el �ltimo mensaje y call� en forma definitiva DESTRUCCION TOTAL EN 30 SEGUNDOS, y notaron que el cerebro ya no se cre�a la historia de tener un cuerpo que sirva para algo, pero hab�an entrado en los recuerdos de los pormenores de la destrucci�n del CCP, tomando posici�n el enemigo en el territorio defendido.

"Lo tenemos".

Afuera, tres autos que se hab�an acercado sigilosamente al per�metro, giraron en 180 grados, y comenzaron la huida desesperada; algo en el display de cristal de su radio, indic� eso que no quer�an ver, DESTRUCCION TOTAL EN 30 SEGUNDOS, El sanatorio ten�a que desaparecer del retrovisor de la Moni (y r�pido). 

Los amarillos comenzaron a escupir con todo lo que ten�an, destrozando al "artillero" adolescente de la camioneta que hu�a por la avenida principal.  Una baja mas entre las filas de los Banderistas, esta vez el que contestaba in�tilmente los disparos de los amarillos; otra baja in�til en la, tal vez, in�til lucha por conservar un islote de humanismo, contra lo inevitable.  

La Moni esboz� una mueca de risa porque el negro Darwin no se fuera solo, sino con una nutrida escolta de hijos de puta.

Cuatro pares de sustantivos, pugnaban por salir de alg�n lugar oscuro del neocortex de Estevez, Jorge Alberto.  Argentino.  Nombre de guerra: Darwin, Beto en la secundaria, Jorgito para los abuelos.

Los datos rebotaban tres veces y sal�an de a uno, cedi�ndose el paso, como prisioneros de una batalla perdida.

El m�dico viejo a�n dudaba de algo que no cerraba del todo, mientras comenzaba la grabaci�n de los datos en copias de seguridad, por las dudas.  "Es como sacarle un juguete a un chico", dijo el agente pol�tico de la derecha, para nuevas risas.  "ES un chico", dijo el m�dico viejo en alusi�n a la edad de Darwin en el sue�o.  

De la guardia informaban por los microtransmisores, que hab�an abortado una misi�n de rescate de los fan�ticos autotitulados "los Banderistas", sin que llegaran a intentaran nada.

El anteojudo, se apuraba sin entender porqu�, en sacar todo lo posible y grabarlo, como con un miedo adquirido con los a�os de hacer lo mismo, con un convencimiento que no evitaba ese gusto amargo de mierda, a veces.  La letra "A" del teclado respond�a al pelo ahora.

Ten�an una direcci�n donde hab�a un supuesto �BUNKER?, Los nombres de unos �SUB-PRINCIPALES? a cargo, las claves de Ingreso de lectura instant�nea, la �VISION NOCTURNA?. Cada verbo o sustantivo los dejaba con mas dudas �HORMIGAS?.  �THEOLOGICAL SOFT CORP.?

Pens� el mas joven que hab�an trabajado tanto para nada, que eso que estaban rescatando no pod�a ser.  No puede haber como jer�rquicos sub-principales, sino principales.  Destruir las cosas con Hormigas es il�gico.

El Fugitivo no pod�a moverse, pero de haber podido, su hubiera sentado en la camilla, hubiera re�do seguramente al ver la imagen que apareci� en las pantallas, serena.  Un primer plano, grabado en forma impecable,  editado empalmado, como si hubieran dispuesto de mucho tiempo y t�cnica para hacerlo; tal vez ni Darwin supiera que ten�a eso grabado en su cerebro, como tampoco sab�a su nombre verdadero.  El fugitivo no mostraba maldad ni venganza alguna por la muerte.  Alg�n m�dico vio una imagen grabada, otros, presumieron una emisi�n radial de los de la operaci�n de rescate abortada, y alg�n m�stico vio un alma, que habl� como si lo hiciera para un p�blico de programa televisivo de concursos.


 "Se�ores, no tengo idea de mi condici�n actual de vida, aunque lo imagino, poco importa, como poco importa que hayan violado mis cerraduras internas y roto mi cuerpo, Los pormenores de la operaci�n para la que fui programado, como otros, quedar�n aqu�, incluso si han llegado a grabar algo, dado que no hay medio mec�nico, magn�tico u �ptico que pueda resistir los 18000 grados.  Si no hubieran llegado hasta ac�, yo hubiera muerto de viejo en unos cuantos a�os, sin enterarme siquiera que llevaba una pila de Plutonio de 1.2 Kilotones y que se activaba en estas circunstancias.  De mas est� decir que lamentamos la ca�da de los  inocentes, pero las reglas del juego no las hemos hecho nosotros"


La imagen se apag�, el m�dico joven mir� al resto levantando las cejas y abriendo la boca, el viejo escupi� el cad�ver mostrando hasta donde hab�a perdido y volvi� la vista para ver en la pantalla el reloj que marcaba exactamente, como el cuerpo lo hab�a previsto, las 15:53:51, en el mismo instante en que la luz convirti� todo lo viviente en unas cuadras a la redonda, en ALMAS.



DIC'93


414 l�neas
4819 palabras

                           * * * * *




