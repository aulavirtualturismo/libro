##La Confederación 
El primer viaje, la primera gran expedición marítima de la que se tiene registro -por lo menos en occidente- es el viaje
del Argo.  El tiempo oficia -siempre- de atenuante de errores y engrandecimiento de hazañas,
porque opera con la lógica de la canción 

>*"recuerdos que mienten un poco, siempre fué asi"*  

Registro que en un principio fuera musical, luego oral y finalmente degradara hasta el nivel escrito. 
Lamentablemente, los griegos carecían de notación musical, por lo que dejaron mucha filosofía, teatro o poesía,
pero nada de música.  Me gusta pensarlos como jazzeros que iban improvisando, pero es solo un cuelgue mío.

Leemos en toda la literatura clásica canciones que se cantaron en honor de dioses, reyes y héroes.  Pocas canciones de
amor, es cierto.  Pocas mujeres en las canciones.  De ninguna de esas canciones sobrevivió una partitura.

Es lógico y esperable pensar canciones al **Argo**.

Las fuentes son dispares respecto de la cantidad y nombres de los argonautas.  **Robert Graves** sostiene que esto
se origina en una la necesidad puntual.  Cada una de las naciones que formaban la confederación griega, ufanarse de
haber contribuido al viaje, una explicación *-económico marxista-* que justifica la lucha contra los piratas del Egeo. 

Asumir un viaje fantástico implica asumir fantásticos a los protagonistas. 

Entre los célebres tripulantes, encontramos a los dióscuros **Castor** y **Polux**, que al ser hermanos de
la Helena mítica de la guerra de Troya nos coloca el un dilema espacio temporal. Otro integrante es **Heracles** el mismísimo
Batman, Súperman y todos los héroes de Marvel juntos.   El músico **Orfeo** tiene la función de ningunear el canto de las
sirenas, a estos se suman varios actores de reparto.  Un hecho curioso, solo una mujer **Atlalanta** se cuenta a bordo,
hecho este que nos lleva a imaginar *prácticas afectivas* -al menos- poco convencionales.  A esto se suma que
otro de los tripulantes fuera mujer y después hombre. El sabio Tiresias también parece haber estado, que fuera hombre, mujer
dependiendo -aparenteente- del estado de ánimo.

Todo parece indicar que cada vez que los rapsodas cantaban las aventuras del **Argo**, cumplían con el agregado al
listado de su héroe local.

**Diómedes**, comandaba el trirreme.  El capitán Diómedes pasa a llamarse Jasón, vaya uno a saber en
virtud a que comodidad estética.  Las esdrújuas son mas difíciles que las agudas, para rimar en
las milongas que seguramente cantaban los rápsodas.  Pero esto también es cuelgue mío.

El oráculo de Delfos le había vaticinado al rey Pelías que un joven lo destronaría.  La particularidad de este joven
era que le faltaba la **sandalia derecha**.  Mirá que hay que ser retorcido para mandar mensajes, por mas que seas la
Pitonisa del templo de Apolo en Delfos.  Que le costaba mandarle datos mas concretos.  Cada email de la pitonisa es
un rompecabezas, lo que nos lleva a suponer consumos tóxicos poco "caretas".

##Mi pie Izquierdo
La historia se pone interesante justo aquí, ya que los historiadores deducen de ese hecho un comportamiento
que trasciende esta historia hasta nuestros días.

Supone Tucídides que Jasón era un guerrero, puesto que solo los guerreros iban *"calzados"* de un solo pié. 
Esta particularidad demostraría que era una especia de Rambo de la época.  

Los guerreros etolios y luego los plateos en las guerras del peloponeso, adoptaron este comportamiento de combate en
terrenos planos, ya que el pie izquierdo -el que porta el escudo- se apoya en el barro para afirmarse, dejando
el brazo derecho -el que lleva el arma- y la pierna izquierda para agredir al enemigo.  Suponemos llúvias, barro
y enfrentamientos de infanterías ligeras de ropas.

##Los emails del señor
El **pensamiento mágico** trata de encontrar -y encuentra de hecho- relaciones entre **hechos inconexos**, como por
ejemplo destripar un ave y esperar un mensaje divino para una batalla, o rezar un rosario para rendir una materia, por
caso  __"historia de la Grecia antigua"__

El capítulo Numérico del antiguo testamento es hermoso en este aspecto, y lo que los cabalistas judíos posteriores
hacen de esto, más aun.

Desde el mítico viaje del **Argo** han pasado algunos miles de años, pero de Jasón y los etolios, su manera de
empezar las batallas, sobrevive la **"superstición"** de iniciar el avance con el pie izquierdo.

El pensamiento mágico asume que vas a perder la batalla si por casualidad entras en ella con el pié derecho.

Por eso, los ejércitos de todo el mundo inician la marcha con el pie izquierdo, y guardan un paso ordenado, siempre
desde el pie izquierdo.

Aún que no es un buen augurio entrar a hogares amigos con el pie izquierdo, ni levantarse con el pié izquierdo, o
tocar un órgano sexual izquierdo ante el nombramiento de un ser nefasto.  Este **"conjuro"** se realiza para contrarrestar
el alineamiento de **energías negativas** que se producen al invocar un espíritu maligno.  

>"Invoca Lupo" -dice un italiano/a/e- 

>"Creppi"  -contesta el otro/a/e haciendo cuernitos hacia abajo-

##Zurditos Marxistas Homosexuales
En las asambleas de la Revolución Francesa, los radicalizados se sentaban a la izquierda, por lo que pasaron a ser los *zurdos*.





