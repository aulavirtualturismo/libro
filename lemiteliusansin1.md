Title: Lemiteliusansin
Date: 2019-03-20 12:00
Category: Novela SciFi Cordobesa

## Lemiteliusansin

# Capítulo I

El **Leprosario**, como lo nombran los locales, es una oficina pública donde la sociedad revive o reutiliza aquella arcaica costumbre de los treintas, en la que busca deshacerse de los distintos y pocos, en beneficio de la salud de los más y muchos y sus uniformes vidas.  Viejas soluciones para nuevos problemas.

Acercan sus fuselajes al leprosario, en calidad de visitantes; Jacinto, aquel de bondad extrema, junto a Rómulo, esteta, de patología dialecticomaterialista, tan extemporáneo al siglo XXI como un trompo de madera, o un vendedor de velas fuera del exclusivo ámbito de la fiesta del 25 de mayo en la escuela primaria.  

Fuera de lugar y tiempo, como el sustantivo **solidaridad**, que pareciera aludir a una **ART** o **AFJP** o una **EaEaEa PEPE**, u otra **sigla** de este nuevo **siglo**; si, este de acortamiento de palabras, de reducción de rituales desaborizados, de **perros a upa** -alquien debe explicarme esto-, de sacralizad futbolística -esto también-, de nuevos dioses de mercado paciente y prolijamente guionados.  De fervor sobre imbecilidades y desconocimiento de corelaciones del tipo: **Neoliberalismo, calentamiento global y masacres** o **Agrotóxicos y Cancer**

A Rómulo, nos hubiera sido grato hacerlo provenir de **Abdera** u alguna otra vieja polis, o festejar que **"siete ciudades del Helesponto disputan su nacimiento"**, por la comodidad estética de llamarlo **Ròmulo de Abdera** frase mucho mas fuerte que **Rómulo de Barrio Suarez**.   

Fustiga Rómulo la realidad, opone feroz resistencia y reniega, o sea, niega y vuelve a negar, o sea.  Como que **Re-Niega** loco.  Su virtud es observar planes alternativos donde no hay salidas en apariencia, cuando todo está perdido, cuando ya están los compañeros se encuentran enrollando las banderas y otras cosas.

Rómulo resume la dialéctica en su accionar, eĺ es dialéctica que anda, oposiciones de tesis y antítesis, que forman síntesis, que son nuevas tesis, que da inicio al ciclo recursivo.  Rómulo llegó a este planeta a negar la realidad, hacer visible otra forma a los ojos humanos, con muy pocos argumentos, ya que es hombre de palabras muy medidas, ejerce cierto **minimalismo verbal** del que es acusado a diario por su par Odiseo Torres, igual a un dios.  

Damos frecuentemente con individuos que pregonan su verdad, religiosa, política, estética y creen estar en lo correcto.  Rómulo lo hace solo a pedido del público, a la usanza de aquellos que conocen mas de una verdad, o como los que viven cerca del mar.

Rómulo, de hebraico apellido, de compleja dicción, milita la posibilidad de otra realidad económica, o social, o estética de la que propone el sistema apodado **la máquina** por él.  Sostiene, tal vez con exceso de furor e intuición pero carente de rigor científico, que el último bastión a resistir contra el sistema, es aquel emparentado al **placer** y el **dolor** como bien nos anticipara Foucault.  Habiéndose ganado la batalla en tantos aspectos de la vida privada, esta Máquina niega el placer, imponiendo monogamias, formatos fijos del goce de vivir, prohibición de consumo de tóxicos, a la par de la imposición de muy puntuales tóxicos.

Esta **máquina** impone al individuo placeres ordenados y catalogados, de un brevísimo **catálogo de libertades**, que parecen centrarse en visualizar desnudos femeninos.  Por los motivos de lo mas heterogéneos, obliga al individuo a fumar o a creer que debe tener un auto cero kilómetro, o consumir yogurt, o gaseosas a la par del desnudo.  Satura el intelecto con curvas, fronteras donde la carne es ropa y viceversa.  

Por eso **La magia**, la de verdad, pierde valor asombraticio al momento de ocurrir.  Hay perdida de ritual al asombro de la mujer en ropa interior, al aroma, al brillo, al asombro del asombro.

El milagro, ese inexplicable y maravilloso, que conocemos con la denominación: desnudo femenino, es menos de lo que debiera ser, por el uso indebido, por el abuso de la magia para vendernos motos, champúes o números de lotería.

Rómulo siempre fue un fiel seguidor de aquellos franceses contestatarios y tóxicos, dado que el conocimiento contiene cierta toxicidad.  Asegura que lo mismo ocurre con los placeres estéticos.

Su nutrida horda de amigos, asume con pasmosa naturalidad, que los teóricos franceses aludidos no son otros que los filósofos Pinot Noir, Sauvignon Blanc, Merlot y Malbeq, cual dumescos mosqueteros esgrimiendo ideas, en defensa de altísimos ideales.

Aquella perversa máquina, enemiga de nuestros héroes, impone placeres insulsos, mientras niega el derecho individual a quitarle el dolor -aparentemente- inevitable al paciente, al prohibir la eutanasia. 

Al final de la agonía, siempre los "técnicos" llegan con aquella excusa de "tenemos que colocarle la inyección", como si se tratara de algo malo, prohibido.

La máquina Impone un estilo de vida que lleva inexorablemente a patologías puntuales, derivadas exclusivamente del tipo de vida culpable y angustiosa, es natural que las células enloquezcan, cuando el CPU del cuerpo está loco y prefiere morir.  Llegado el momento en que el individuo pudiera optar de motu propio, el final del sufrimiento, lo obliga a cumplir el último rol en beneficio de laboratorios y la industria de la salud, la facturación, y los viajes de "capacitación" de los oncólogos.

Algo como un guión, un rol en la obra de teatro que obliga para ser feliz debes: tener tu casa, tu auto, tu casa de fin de semana, debes veranear en el Caribe, tus hijos deben ir a Disney, debes coleccionar miles de artefactos de los que en cuatro años olvidarás la utilidad.  Si quieres lograrlo, debes ceder veinte horas diarias de vida a la máquina, mientras, fuerza a estos sanos individuos a observar con tristeza a aquellos seres que deambulan sucios, barbudos, borrachos, con su amplia y heterogénea familia perruna orbitándolos alrededor del carrito de supermercado, cargado de 

Cosas, envidiándoles, pero sin saberlo, ese amor que se sienten.  
La libertad también es un bien, por intangibilidad, pasa desapercibida a nuestro corazón, luego, también es envidiable.

Toda aquella ansiedad se aplaca con unos elegantes y legales fármacos, ya que también queda muy bien soltar en las reuniones, ante el “¿Cómo andas?” inicial, que se está mejor desde las tomas del clorazepam o del ribotril, ante los rostros sonrientes del entorno y caras de “que bueno, che” o "viste que fulano cambió el auto” o la novia.

El individuo no sospecha que le queden muy pocas horas de vida.  De vida como la conocía.  Cuando no es un pico de tensión, es un paro, directamente ligado a ese modus vivendi que aceptó y que ni la menor sospecha tiene que dicho estilo no sea propio, sino guionado, en ese afantasmamiento, esa metamorfosis a otra cosa, mas parecida a lo que esperan de él, que a él mismo como salió de fábrica.

Pequeño hijo de puta el sistema este, al que Rómulo llama La Máquina ¿NO?  

Más hijo de puta aún, al hacernos asociar el dolor, precisamente, ellos, la fábrica del dolor, con las profesionales del placer, esto es, las putas.
Interesante tarea la de averiguar quién aprovecha del exceso de libido entre el trabajador, el capitalista, los medios, las tarjetas de crédito, los credos y una larguísima lista de gente que vive del prójimo.  Vampirismo, que le llaman.

Otro singular y periódico -¿o pretendía decir paródico?- frecuentador de la oficina pública, es el deiforme Odiseo Torres, el cual, fiel a su espíritu libre y al tiempo que le sobra para imaginar realidades distintas; propone respecto del vampirismo, una veta no desarrollada o desperdiciada, en tan amplia bibliografía específica.

Odiseo, que jura haber combatido a la par del príncipe en alguna vida pretérita, asegura que Vlad Tepez era capaz de observar perfectamente su imagen reflejada en los espejos, por aquel entonces.  

Odiseo, tal vez sea necesario aclarar, es partícipe de ese tan extraño como ajeno sistema de creencias, que sugiere la reencarnación del alma en nuevas vidas, conforme dejamos deudas pendientes en las anteriores.  No somos quienes para ponerlos en duda, el homo sapiens medio es crédulo, lo prueban los depósitos en los bancos o las publicidades.

A juzgar por la vida actual, viene dejando deudas desde antiguo, puesto que pasivos, es lo único que ha acumulado, a lo largo de los siglos.  Nos recuerda, no es narración; Odiseo jura recordar aquellos tiempos bárbaros, aquellas primeras batallas por la reconquista del santo sepulcro, con una fidelidad que aterroriza mas que los finísimos caninos del príncipe.  Acuden fácilmente a su memoria, formatos de espadas, escudos de armas, nomenclatura de cabalgaduras, las formaciones militares y los asedios de las ciudades, la ausencia de aseo diario -que no forzosamente signifique limpiarse con un periódico-, el nacimiento de los mitos zodiacales.

Hay muchas batallas, mucha sangre en esos relatos.  

De todo tipo.  

Y de todo factor RH.  

Pero que cierta jornada, podemos conjeturar que ya en presencia de la aurora de rosáceos dedos, lo que el príncipe en realidad vio en el espejo, fue formarse -como si de una ambulancia se tratara, por esa perversa y extraña virtud del reflejo- la palabra *Alucard*, adquiriendo, en ese solo y mismo acto, un pantallazo, un flash escénico del siglo XX y XXI, temblando en un rechazo visceral similar al experimentado por el ajo.  Según Odiseo, le ocurrió lo que nosotros vemos en el cine en los relatos de ficción futura, pero allá por el siglo XV, tal vez producto de algún tóxico o algún neurotransmisor distraído, falta de sales, irrigación defectuosa, etc.

El príncipe adivinó, gracias a la magia del espejo, un futuro en que se utilizara espúreamente su bien ganada fama, la de su fortaleza, su longevidad, su exquisito paladar, todo ello involucrado en publicidades de tarjetas de crédito “con Alucard usted puede comprar en veinticuatro cuotas...”, gente que sonríe de forma esquizoide, hermosas niñas que levantan las ancas -no leven anclas, que es otra cosa- y hombres que hinchan pectorales, traban abdominales y hombros, bancos norteamericanos, actores que hacen de gerentitos con corbatas de seda en publicidades, extendiendo no la mano para saludar, sino un plástico, llave de un mundo mas pelotudo, verdaderos chupa sangre y desde entonces, el buen príncipe Vlad ya no utiliza más espejos.  Sacudió la cabeza, para despejarla de aquellas imágenes en el futuro y casi se le escapa un “por dió” que reprime y respira hondo, haciendo cuernitos para abajo, con ambas manos, emulando auxiliares de a bordo, señalando puertas de emergencia al bidigital estilo, solo que con otros dedos.

Hasta los peores malvados tienen códigos, eligen los enemigos con quién enfrentarse y cuando rebajarse hasta aquellos "que no están a la altura del conflicto".  El príncipe, asegura Odiseo, por lo menos tenía código. 

Mas aplaque su temor estimado lector, se trata de palabras de Odiseo Torres, no desprovistas de inventiva, de fantasía, proclive como lo es nuestro héroe, a hermosear la realidad merced al verbo, bien sabemos lo útiles y necesarios que son los bancos, o las tarjetas de crédito, o la cantidad de cosas que se pueden adquirir con ellas.  Símbolos, perfectos mojones delimitadores de la posición topológica que ocupan los individuos respecto de la máquina, cuan dentro, cuan fuera de ella, la libertad de tránsito en una u otra dirección.  O la falta de esa libertad. 

Regresemos de los Cárpatos, para adentrarnos en la oficina pública donde malgasta sus horas el deiforme Sisoco García, retomemos el relato abandonando aquellas heladas cumbres europeas. 
