require(tm)
require(wordcloud)
require(RColorBrewer)
 
 
r <- paste(readLines(file.path("/root/Documents/","macri.txt")), collapse=' ')
r <- gsub("[ft.,;:`'\"\(\)<>]+", " ", r)
words <- tolower(strsplit(r, " +")[[1]])
 
words <- table(words)
# remove words with _bad_ chars (non utf-8 stuff)
words=words[nchar(names(words), "c")==nchar(names(words), "b")]
# remove words shorter then 4 chars
words=words[nchar(names(words), "c")>3]
# remove words accuring less than 5 times
words=words[words>4]
 
# create the image
png("cloud.png", width=580, height=580)
pal2 <- brewer.pal(8,"Set2")
wordcloud(names(words), words, scale=c(9,.1),min.freq=2,
           max.words=Inf, random.order=F, rot.per=.3, colors=pal2)
dev.off()