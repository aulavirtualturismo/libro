                          LA OTRA PUERTA

                                   HOBBY: (dec�a la solicitud  de 
                                   empleo) y el Juanchi contest�:
                                   "colocar hojitas de afeitar en 
                                   los toboganes de los jardines
                                   de infantes"

                                   Un adolescente aterriza en  la 
                                   plaza  frente  al  Kremlin  de 
                                   cuando   el  Kremlin  era   el 
                                   Kremlin,   otro,   mueve    un 
                                   sat�lite  estrat�gico  de   la  
                                   NASA con una PC y un paraguas.  
                                   Lo que sigue no es lo  imposi-
                                   ble que pareciera. 

     El Manhattan era un tanto mas rico en mensajes que su  hom�-
logo diurno, el "Revoll Village Music", tal vez por ser mas �vido 
de venganzas, o por tener mas cosas que cuidar, o mas de las  que 
vengarse.  En el espejo que serv�a de pared de la barra, solo los 
banderistas alcanzaban a leer la frase "Subsecretar�a de  alcoho
les",  tal vez por alg�n pigmento adicionado a la pintura  de  la 
contracara  del vidrio, haciendo que las luces chocaran de  forma 
especial,  como en los hologramas.  Mas adelante, llegando a  los 
reservados,  una  cortina, de las pesadas, los recib�a  con  otra 
frase "Subsecretar�a de Rascado y afines", sin especificar  hasta 
donde  llegaba  aquella  generalizaci�n de:  "afines",  tal  vez, 
dej�ndolo  librado a las "libreinterpretaciones" o  �asociaciones 
libres?. 

     Esto �ltimo tiene su raz�n de ser en funci�n de los siguien
tes mensajes mas adentro, "Subsecretar�a de Coitos non sanctos  y 
fellatios"  y "Secretar�a general de coito ortodoxo simple",  con 
lo  que  parec�a quedar agotada la variedad de  los  productos  a 
consumir en el lugar, o que las distribuciones no fueran del todo 
taxativas y concluyentes. 

     De todos modos, la lectura de estos afiches no era accesible 
al p�blico en general, por lo que no alejar�a p�blico que buscara 
otra mercader�a, sino solo a aquellos que llegaban para acercarse 
hasta despu�s de la barra, detr�s del ba�o con la figurita humana 
de polleras, donde la pared hablaba como lo hac�a en la disquer�a 
"traspasando  esta, estar� Ud. del otro lado", en el  mismo  tono 
cort�s  y  amable.   Solamente a los  banderistas  que  quisieran 
bajar. 

     Muy pocos de los banderistas ten�an conocimiento del  signi-
ficado  de aquellas palabras encadenadas, como toda frase  clave, 
como  aquel otro "Abrete s�samo"; solo llegaban a notar  que  las 
puertas  de  ambas  entradas se abr�an  cuando  pasaban.   Cuando 
alguien  le�a o recordaba ya de memoria esa sincron�a de  letras, 
se  activaba  un  receptor de sinton�a  extremadamente  fina  que 
interpretaba las intenciones del supuesto lector.  Innecesario es 
recordar  las  consecuencias  inmediatas de  aquel  que  quisiera 
traspasar cualquiera de esas dos entradas sin ser invitado por su 
propia lectura.  Los detalles ser�an demasiado morbosos.

     Al  llegar  ante la entrada, todos recordaban  la  frase  de 
memoria, salvo los reci�n injertados, que habr�an bajado dormidos 
la primera vez, y se encontraban de repente con esa nueva  facul-
tad de abrir paredes con el pensamiento y ver cosas de noche.  En 
una  sola ocasi�n no se accionaba el dispositivo de  apertura,  y 
era  la  posible intromisi�n de un no-lector, esa  cercan�a,  esa 
inmediatez de alguien tambi�n se emite en el pensamiento, conjun-
tamente  con  el deseo de trasponer los l�mites del arriba  y  el 
abajo,  por lo cual, el sistema de defensa chequeba la  no-concu-
rrencia  de  esos  dos "sentimientos" en forma  conjunta,  el  de 
traspasar los l�mites con el de tener alguien al lado. 

     Los  encargados de los sistemas, los que llamaban  ratas  de 
computadora,  por  oposici�n a las viejas ratas  de  bibliotecas, 
ten�an todo estudiado como para ahorrar el batall�n de centinelas 
con  cerebro de tero, que deber�an tener detr�s de los  monitores 
(faltando  este  sistema de entrada-salida), para  controlar  las 
v�as  de  acceso  al bunker.  De esa forma, se  contaba  con  mas 
personal  en funciones mucho mas "Humanas", como las siembras  de 
bombas,  los  reclutamientos de nuevos integrantes,  las  muertes 
enemigas,  leer las revistas policiales, tocar la  guitarra  para 
sacar  viejos  temas de Bob Dylan, o descifrar  los  secretos  de 
ciertos escritos herejes de cuando la historia a�n no lo era. 

     Esta  tarea  rutinaria de controlar la puerta, se  la  pod�a 
confiar al buen juicio de un sistema bien hecho, para ello hab�an 
robado  las mejores computadoras de un embarque que llegaba  para 
el  ministerio de defensa, en el puerto de Buenos  Aires.   Alg�n 
gil  de turno, en la burocracia, pretendi� ahorrarse personal  de 
seguridad en el transporte y le sali� un poco mal. 

     Pasando  los  ba�os  de se�oritas,  despu�s  de  la  segunda 
"lluvia  de  abajo", aguardaba la frase a ser  le�da  para  poder 
entrar.  Justo antes de esa entrada, unos alambrecitos dispuestos 
en espiral, de manera estrat�gica, esperaban los pensamientos  de 
los que pasaran por ah�, haciendo de antena, transmitiendo  desde 
ah� con un coaxil muy delgado y preciso, las leves ondas hertzia-
nas recibidas en una frecuencia alta en demas�a, desde los axones 
de cualquier sistema nervios central que estuviera debajo de esos 
alambrecitos.  Esos pulsos de ultrafrecuencia llegaban al codifi-
cador  unos metros mas all�, que los transformaba en  informaci�n 
digital  y  la enviaba al ordenador encargado de  la  vigilancia; 
este distingu�a solamente dos formas de pulsos entre las astron�-
micas posibilidades, los que correspond�an a la secuencia  l�gica 
"traspasando esta, estar� Ud. del otro lado" y los que NO corres-
pond�an, por lo que la puerta reaccionaba de dos formas, permane-
ciendo  cerrada, o abri�ndose al Mensaje, que alteraba el  Status 
Quo.  La combinaci�n binaria 0-1, SI o NO. 

     El  cerebro  rastreador de IDEM(cerebros),  formado  por  el 
conjunto de alambres, cables, relay y motores, era simplemente un 
programa ideado para descubrir la cadena de caracteres que  coin-
cidiera con la otra cadena escrita en la pared, ninguna  sofisti-
caci�n  t�cnica  del otro mundo, pero como todas  las  cosas  que 
sirven,  era sencilla, simple pero bien usada, como las gentes  o 
las  actitudes de las gentes, pueden ser muy sencillas,  pero  si 
est�n bien aplicadas, sirven o se justifica su existencia. 

     Para ello, dicho programa de seguridad, chequeaba entre  las 
miles de variantes en que puede encadenarse un idioma en particu-
lar, la revisaba cuarenta y dos veces (por las dudas),  veintiuna 
pasadas en un sentido y veintiuna a la inversa.  Algo que con  la 
voz no se puede hacer, porque entran a tallar, tonadas  riojanas, 
con  porte�as,  zezeos y cancherismos al hablar, sin  contar  los 
timbres  y dem�s yerbas, o la posibilidad de resfr�os o  cigarri-
llos  entre los labios.  As� encontraba la secuencia  il�gica  en 
cuesti�n, entre algunas como: 

-"Non intratur in veritatem, nisi per caritatem" 
     
     � 

-"Maradona  no  deber�a haber pateado ese tiro  libre  con  tanto  
efecto" 

     � 

-"La noche se puso �ntima, como una peque�a plaza." 

     � 

-"En el principio, era el verbo..." 


     �

-"Act�a de forma tal que el m�ximo de tu obrar  pueda  servir  de
ley universal"

     Y  miles  de felices frases, v�lidas todas en  su  contexto, 
pero in�tiles para bajar al bunker de los banderistas,  afortuna-
damente, "y si algo se libera mediante verbo, debe ser porque  el 
verbo lo sujet�" y todas esas pelotudeces. 

     El banderista dispuesto a transponer los l�mites del  arriba 
y el abajo, deb�a leer forzosamente el mensaje, como cuando  pide 
una  gaseosa espec�fica y no otra, sin saber porqu�.  Ese  cartel 
estaba dispuesto a la usanza de los afiches de los cigarrillos, y 
las  sonrisas de las ni�as de las gaseosas.  Un  mensaje  er�tico 
mas, uno mas en la erotizada civilizaci�n para vender. 

     Pero  as�  como  pod�an leer esos raros  hologramas  en  esa 
longitud  de  onda, tambi�n ten�a sus desventajas, dado  que  los 
banderistas no pod�an ver los monitores comunes de 625 lineas por 
segundo, sino en una velocidad mucho mas lenta, por lo que  nece-
sitaban adaptar todos los televisores del bunker, para poder  ver 
lo  poco  que los canales de televisi�n dec�an, si bien  era  una 
desventaja no poder ver televisi�n, perdi�ndose todo el cargamen-
to de cosas que esta dice por d�a, noticias, informaciones, etc.; 
por  otro  lado,  ellos se ahorraban amargo trabajo  de  ver  las 
idioteces  que  esta dec�a, noticias, informaciones... o  sea  lo 
mismo. 

     Por  otro  lado, por primera vez en la  historia,  un  grupo 
heterog�neo de gatos, observaba con pasmosidad felina, una novela 
venezolana en un televisor, en un par de almohadones  compartidos 
frente  a la pantalla, y la entornada de cabezas ante una  escena 
de  peligro,  o sabore�ndose ante las escenas de comidas  de  los 
boludos programas del mediod�a, o el encrespamiento de los  pelos 
del  lomo  ante  la aparici�n  de esos documentales  de  la  vida 
silvestre en el desierto del Kalahari, filmado por esos tipos que 
se  pasan  siglos al sol esperando que levante vuelo  un  imb�cil 
p�jaro en extinci�n; extinci�n a la cual contribuir�an con agrado 
los felinos acostados en los almohadones reposando, a juzgar  por 
los rostros.  A esos gatos les fue realizada la operaci�n quir�r-
gica inversa a la de los banderistas. 

     El  sub-principal Gast�n, el oficial a cargo de la  divisi�n 
gen�tica, hab�a hecho el proceso inverso, como prueba, injertando 
gatos  con delgadas capas de cerebros humanos, pero los  rechazos 
eran de consideraci�n, con una alta tasa de mortalidad.  Tal  vez 
los  humanos  ten�an altos �ndices de toxinas,  como  para  poder 
ceder  material  celular, y no a la inversa; o  simplemente,  los 
humanos  a los que extirpaban las capas para transplante  corres-
pond�an todos a la misma categor�a: enemigos (todos los  sacrifi-
cados  eran  enemigos), por lo que es de presumir que en  vez  de 
masa encef�lica tuvieran masa f�lica.  Con algo de paciencia,  en 
algunos a�os contar�an con gatos que pudieran superar las  prime-
ras  etapas  de la actividad l�dica primaria,  con  injertos  muy 
peque�os  y la evoluci�n muy lenta.  El gato a�n contaba con  una 
ventaja a su favor, las siete vidas, que le permitir�an fallar en 
el cumplimiento de los estadios por los que debe pasar el indivi-
duo  INDEFECTIBLEMENTE,  tal vez en forma un  tanto  desordenada, 
animal, y sortear todo aquello de los sensoriomotores, las opera-
ciones  formales,  las operaciones l�gicas, y por otro  lado,  el 
chuparse los dedos (las u�as en este caso), gozar haciendo caca y 
enamorarse  de  la madre (cosa que los gatos  suelen  hacer  para 
solucionar  el Edipo.  Los pocos gatos observados  empiricamente, 
no tienen ning�n problema en voltearse a la madre). 

     La  entrada nocturna del Bunker, como toda puerta, era  tam-
bi�n  una  negaci�n, una prohibici�n generalizada, a la  cual  se 
hac�an  ciertas excepciones por leer esa frase  escrita,  cumplir 
con el mito, obedecer a los tab�es de la sociedad tribal, con sus 
ritos de iniciaci�n, sus m�dicos brujos, sus circuncisiones, etc.  
Una negaci�n espec�fica que implica muchas, que incluye la  acep-
taci�n (en su faz negativa), que son tambi�n muchas aceptaciones.  
Es dif�cil aceptar la amargura de ser otra cosa, no entrar en  la 
perfecci�n del circuito donde est� todo diagramado  prolijamente, 
todo dispuesto tal y como debe ser, pero no por ello deja de  ser 
ineludible.  

     El  Gast�n era el cl�sico tipo sereno que se duerme  mirando 
las  pel�culas de mayor suspenso o acci�n, que confesaba  que  el 
deporte  mas violento que hab�a practicado en su vida hab�a  sido 
el  TRUCO,  y es cierto que en ocasiones de  alternar  truco  con 
alcohol  se torna mas violento a�n.  La tranquilidad del  vientre 
materno que significaba el bunker, lo dejaba trabajar, lo empuja-
ba  a  hacerlo, no pod�a estar mejor, con  toda  esa  informaci�n 
gratuita,  con techo y comida, casi un burgu�s con la sola  dife-
rencia  de la persecuci�n sobre los hombros, el resto, como  todo 
tipo  normal  de la �poca, alg�n amor no permitido del  todo  con 
alguna  de  las  chicas de la sociedad cerrada  que  lo  rodeaba, 
similar  a  como ocurre el amor en todos los  casos,  los  amores 
siempre  ocurren en un �mbito que no excede las diez personas  de 
los alrededores, salvo contados casos que aman a cuanta mujer  (u 
hombre) se le presente delante.  Hay casos tambi�n que se  enamo-
ran  de  lo que pase por adelante, sin atender a  raza,  credo  o 
bander�a  pol�tica, menos a�n, al sexo que muestre el/la  que  se 
presente adelante. 

     Las dos entradas del bunker eran los brazos de la gran  mam�
que los esperaba con la leche caliente, las tostadas,  ret�ndolos 
porque  llegaban tarde o no hac�an los deberes.  

     Cuando  llegaban demasiado agitados los cobijaba en la  sala 
de  estornudar, donde algunos pasaban gran parte de la vida.   La 
sala  estaba especialmente equipada con aislante ac�stico en  las 
paredes, para evitar molestias a los dem�s chicos.  Dentro de  la 
sala de estornudar ten�an todo lo que se pudiera necesitar,  todo 
lo que se pudiera leer, o ver, o comer con la sola diferencia  de 
la aislaci�n del resto.

     Detalles mas o menos la historia siempre es la misma,  todas 
las  variantes de lo que puede ocurrir en este mundo, se  podr�an 
contar con los dedos, a grosso modo; las formas en que las mam�es 
reciben a los hijos.  La forma en que se forman las nuevas madres 
y  cumplen sus funciones, la forma en que todos buscan  una  mam� 
donde cobijarse, donde meterse para regresar a aquello otro.     
